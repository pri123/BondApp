<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
//use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller {

  function __construct() {
    // Construct the parent class
    parent::__construct();
    $this->load->database();
    $this->load->model("common_model");
    $this->load->library('curl');
    $this->load->library('upload');
    $this->load->helper('file');
    $this->load->helper('url');
    // Load facebook library
    //        $this->load->library('facebook');
    //Load user model
    //        $this->load->model('user');
  }

  /**
   * Register user
   */
 
  public function register_post() {

    try {

    if ($this->post('name') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter your name'), 200);
    } else if ($this->post('mobileno') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter mobile number'), 200);
    } else if ($this->post('gender') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter gender'), 200);            
    } else if ($this->post('age') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter age'), 200);            
    } else if ($this->post('age_range') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter age range'), 200);
    } else if ($this->post('device') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter device'), 200);
    } else if ($this->post('deviceid') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter device id'), 200);
    } else if ($_FILES['user_profileimage'] == "") {
            $this->response(array('status' => false, 'message' => 'Please send profile image'), 200);
    } else {

      if (isset($_FILES['user_profileimage']) && !empty($_FILES['user_profileimage'])) {
        $uploaddir = './uploads/profileimages/';
        $file_name = underscore($_FILES['user_profileimage']['name']);
        $path = $_FILES['user_profileimage']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $user_img = rand() . '_profile.' . $ext;
        $uploadfile = $uploaddir . $user_img;

        if (move_uploaded_file($_FILES['user_profileimage']['tmp_name'], $uploadfile)) {

          $name  = $this->post('name');
          $pieces = explode(" ", $name);
          if(count($pieces) >1){
            $firstname = $pieces[0];
            $lastname = $pieces[1];
          } else {
            $firstname = $pieces[0];
            $lastname = '';  
          }
          $range = $this->post('age_range');
          $myArray = explode('-', $range);
          $user_age_range_min = $myArray[0];
          $user_age_range_max = $myArray[1];

          $userdata = array(
            'user_firstname' => $firstname,
            'user_lastname' => $lastname,
            'user_gender' => ucfirst($this->post('gender')),
            'user_age' => $this->post('age'),
            'user_age_range_min' => $user_age_range_min,
            'user_age_range_max' => $user_age_range_max,
            'user_description' => $this->post('description'),
            'user_interest'    => 'Both',
            'user_verified' => $this->post('login_verified'),//for OTP donesend 1
            'user_country_code' => $this->post('countryCode'),
            'user_mobileno' => $this->post('mobileno'),
            'user_device' => $this->post('device'),
            'user_deviceid' => $this->post('deviceid'),
            'user_profileimage' => $user_img,
            'user_created_at' => $this->post('created_at')
          );

          if($this->post('facebook_id') != '') {
            $userdata['user_facebook_id']  = $this->post('facebook_id');
            $userdata['user_logintype']  = '1';
          } else{
            $userdata['user_facebook_id']  = NA;
          }

          if (isset($_FILES['user_image1']) && !empty($_FILES['user_image1'])) {
            $uploaddir = './uploads/profileimages/';
            $file_name = underscore($_FILES['user_image1']['name']);
            $path = $_FILES['user_image1']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $user_img1 = rand() . '_image1.' . $ext;
            $uploadfile1 = $uploaddir . $user_img1;

            move_uploaded_file($_FILES['user_image1']['tmp_name'], $uploadfile1);
            $userdata['user_image1'] = $user_img1;  
          } 
                  
          if (isset($_FILES['user_image2']) && !empty($_FILES['user_image2'])) {
            $uploaddir = './uploads/profileimages/';
            $file_name = underscore($_FILES['user_image2']['name']);
            $path = $_FILES['user_image2']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $user_img2 = rand() . '_image2.' . $ext;
            $uploadfile2 = $uploaddir . $user_img2;

            move_uploaded_file($_FILES['user_image2']['tmp_name'], $uploadfile2);
            $userdata['user_image2'] = $user_img2;  
          } 
                  
          if (isset($_FILES['user_image3']) && !empty($_FILES['user_image3'])) {
            $uploaddir = './uploads/profileimages/';
            $file_name = underscore($_FILES['user_image3']['name']);
            $path = $_FILES['user_image3']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $user_img3 = rand() . '_image3.' . $ext;
            $uploadfile3 = $uploaddir . $user_img3;

            move_uploaded_file($_FILES['user_image3']['tmp_name'], $uploadfile3);
            $userdata['user_image3'] = $user_img3;  
          } 
                  
          $res = $this->common_model->insert(TB_USER, $userdata);
          if ($res) {
            //$data = array( 'user_id' => $this->db->insert_id() );   

            $data = $this->common_model->select('*', TB_USER, array('user_id' => $this->db->insert_id() ) );
            $datas = array(
                'id' => $data[0]['user_id'],
                'firstname' => $data[0]['user_firstname'],
                'lastname' => $data[0]['user_lastname'],
                'mobileno' => $data[0]['user_mobileno'],
                'gender' => $data[0]['user_gender'],
                'age' => $data[0]['user_age'],
                'age_range_min' => $data[0]['user_age_range_min'],
                'age_range_max' => $data[0]['user_age_range_max'],
                'description' => $data[0]['user_description'],
                'profileimage' => base_url().'uploads/profileimages/'.$data[0]['user_profileimage'],
                'subscription' => $data[0]['user_subscription'],
                'deviceid' => $data[0]['user_deviceid']
              );

            $remainingPing = $this->common_model->select('*', TB_USERPING, array('userping_user_id' => $datas['id']) );

            if(count($remainingPing) > 0){
              $pingCount = $remainingPing[0]['userping_remainingCount'];
            }else{
              //get free ping data
              $freePing = $this->common_model->select('*', TB_FREEPING, array());
              $ping_limit = $freePing[0]['ping_limit'];
              // $ping_hr    = $freePing[0]['ping_hr'];

              $data1['userping_user_id']  = $datas['id'];
              $data1['userping_pingCount'] = 0;
              $data1['userping_remainingCount'] = $ping_limit;
              $data1['created_at'] = $this->post('created_at');
              $res = $this->common_model->insert(TB_USERPING, $data1);
              $pingCount = $ping_limit;
            }

            $this->common_model->update(TB_USER, array('user_mobileno' => $datas['mobileno']), array('user_loginstatus' => 1));
            $this->response(array('status' => true, 'message' => 'User added sucessfully go to map screen', 'data' => $datas , 'pingCount' => $pingCount ));
          } else {
            $this->response(array("status" => false, "message" => "Something went wrong. !!"));
          }           

        }  
      }
    } 

    } catch(Exception $e) {
      echo 'Message: ' .$e->getMessage();
    }    
  }


  public function login_post() {

    if($this->post('flagtype') == 0 ){
      if (trim($this->post('mobileno')) == "") {
        $this->response(array('status' => false, 'message' => 'Please enter mobile number'), 200);
      }

      $checkMob = $this->common_model->select('*', TB_USER, array('user_mobileno' => $this->post('mobileno')));
     
      if (count($checkMob) > 0) {

        $checkActive = $this->common_model->select('*', TB_USER, array('user_mobileno' => $this->post('mobileno'),'user_status' => '0'));

        if (count($checkActive) > 0) {

          $userArrUpdateDevice = $this->common_model->select('*', TB_USER, array('user_mobileno' => $this->post('mobileno') ,'user_deviceid' => $this->post('deviceid')));
          
          if(count($userArrUpdateDevice) > 0) {

            $this->common_model->update(TB_USER, array('user_mobileno' => $this->post('mobileno')), array('user_loginstatus' => 1));

             $token = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $userArrUpdateDevice[0]['user_id'])); 

             if(count( $token) > 0) {
                $tokens_remain  = $token[0]['tokens_remain'];
             } else {
                $tokens_remain  = '0';
             }

            $data = array(
              'id' => $userArrUpdateDevice[0]['user_id'],
              'firstname' => $userArrUpdateDevice[0]['user_firstname'],
              'lastname' => $userArrUpdateDevice[0]['user_lastname'],
              'mobileno' => $userArrUpdateDevice[0]['user_mobileno'],
              'gender' => $userArrUpdateDevice[0]['user_gender'],
              'age' => $userArrUpdateDevice[0]['user_age'],
              'age_range_min' => $userArrUpdateDevice[0]['user_age_range_min'],
              'age_range_max' => $userArrUpdateDevice[0]['user_age_range_max'],
              'description' => $userArrUpdateDevice[0]['user_description'],
              'profileimage' => base_url().'uploads/profileimages/'.$userArrUpdateDevice[0]['user_profileimage'],
              'subscription' => $userArrUpdateDevice[0]['user_subscription'],
              'deviceid' => $userArrUpdateDevice[0]['user_deviceid'],
              'token_remain'  => $tokens_remain
            );

            $remainingPing = $this->common_model->select('*', TB_USERPING, array('userping_user_id' => $data['id']) );

            if(count($remainingPing) > 0){
              $pingCount = $remainingPing[0]['userping_remainingCount'];
            }else{
              //get free ping data
              $freePing = $this->common_model->select('*', TB_FREEPING, array());
              $ping_limit = $freePing[0]['ping_limit'];
              // $ping_hr    = $freePing[0]['ping_hr'];

              $data1['userping_user_id']  = $data['id'];
              $data1['userping_pingCount'] = 0;
              $data1['userping_remainingCount'] = $ping_limit;
              $data1['created_at'] = date('Y-m-d H:i:s');
              $res = $this->common_model->insert(TB_USERPING, $data1);
              $pingCount = $ping_limit;
            }

            if($data['subscription'] == 0) {
              $this->response(array('status' => true, 'message' => 'Login successful', 'data' => $data,'edittype' =>'0', 'pingCount' => $pingCount ), 200); 
            } else {

                if($data['subscription'] == 1) {
                  $purchase = $this->common_model->select('*', TB_PURCHASE, array('user_id' => $data['id'])); 
                  
                  $paid = array('start_time'      => $purchase[0]['start_time'], 
                                'end_time'       => $purchase[0]['end_time']
                               ); 

                } else {
                  $purchase = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $data['id'])); 

                  $paid = array('start_time'      => $purchase[0]['tokens_start_time'], 
                                'end_time'       => $purchase[0]['tokens_end_time']
                               );
                }              

              $this->response(array('status' => true, 'message' => 'Login successful', 'data' => $data ,'membership'=> $paid,'edittype' =>'0', 'pingCount' => '' ), 200); 
            }

          } else {

            //if($this->post('user_flag') == 1) {

              $this->common_model->update(TB_USER, array('user_mobileno' => $this->post('mobileno')), array('user_deviceid' => $this->post('deviceid'), 'user_loginstatus' => 1, 'user_verified' =>1));

              $userArrUpdateDevice1 = $this->common_model->select('*', TB_USER, array('user_mobileno' => $this->post('mobileno') ,'user_deviceid' => $this->post('deviceid')));

              $token = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $userArrUpdateDevice1[0]['user_id'])); 

             if(count( $token) > 0) {
                $tokens_remain  = $token[0]['tokens_remain'];
             } else {
                $tokens_remain  = '0';
             }

              $data = array(
                'id' => $userArrUpdateDevice1[0]['user_id'],
                'firstname' => $userArrUpdateDevice1[0]['user_firstname'],
                'lastname' => $userArrUpdateDevice1[0]['user_lastname'],
                'mobileno' => $userArrUpdateDevice1[0]['user_mobileno'],
                'gender' => $userArrUpdateDevice1[0]['user_gender'],
                'age' => $userArrUpdateDevice1[0]['user_age'],
                'age_range_min' => $userArrUpdateDevice1[0]['user_age_range_min'],
                'age_range_max' => $userArrUpdateDevice1[0]['user_age_range_max'],
                'description' => $userArrUpdateDevice1[0]['user_description'],
                'profileimage' => base_url().'uploads/profileimages/'.$userArrUpdateDevice1[0]['user_profileimage'],
                'subscription' => $userArrUpdateDevice1[0]['user_subscription'],
                'deviceid' => $userArrUpdateDevice1[0]['user_deviceid'],
                'token_remain'  => $tokens_remain
              );

              $remainingPing = $this->common_model->select('*', TB_USERPING, array('userping_user_id' => $data['id']) );

              if(count($remainingPing) > 0){
                $pingCount = $remainingPing[0]['userping_remainingCount'];
              }else{
                //get free ping data
                $freePing = $this->common_model->select('*', TB_FREEPING, array());
                $ping_limit = $freePing[0]['ping_limit'];
                // $ping_hr    = $freePing[0]['ping_hr'];

                $data1['userping_user_id']  = $data['id'];
                $data1['userping_pingCount'] = 0;
                $data1['userping_remainingCount'] = $ping_limit;
                $data1['created_at'] = date('Y-m-d H:i:s');
                $res = $this->common_model->insert(TB_USERPING, $data1);
                $pingCount = $ping_limit;
              }

              if($data['subscription'] == 0) {
                $this->response(array('status' => true, 'message' => 'Login successful', 'data' => $data,'edittype' =>'0', 'pingCount' => $pingCount ), 200); 
              } else {
                
                  if($data['subscription'] == 1) {
                    $purchase = $this->common_model->select('*', TB_PURCHASE, array('user_id' => $data['id'], 'status' => 1)); 

                    $paid = array('start_time'      => $purchase[0]['start_time'], 
                                  'end_time'       => $purchase[0]['end_time']
                                 ); 
                  } else {
                    $purchase = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $data['id'])); 

                    $paid = array('start_time'      => $purchase[0]['tokens_start_time'], 
                                  'end_time'       => $purchase[0]['tokens_end_time']
                                 );
                  }  

                $this->response(array('status' => true, 'message' => 'Login successful', 'data' => $data ,'membership'=> $paid,'edittype' =>'0', 'pingCount' => $pingCount ), 200); 
              }

          } 

        } else {

          $this->response(array('status' => false, 'message' => 'Your Account is Deactive','edittype' =>'3'), 200); 
        } 

      } else {
        $this->response(array('status' => false, 'message' => 'Go to edit profile','edittype' =>'1'), 200);
      } 

    }

    if($this->post('flagtype') == 1 ){
      
      if ($this->post('facebook_id') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter facebook id'), 200);
      }

      $userFacebook = $this->common_model->select('*', TB_USER, array('user_facebook_id' => $this->post('facebook_id')));
      
      if(count($userFacebook) > 0) {

        $checkActive = $this->common_model->select('*', TB_USER, array('user_facebook_id' => $this->post('facebook_id'),'user_status' => '0'));

        if (count($checkActive) > 0) {

          $this->common_model->update(TB_USER, array('user_facebook_id' => $this->post('facebook_id')), array('user_loginstatus' => 1, 'user_deviceid' => $this->post('deviceid') ));

             $token = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $checkActive[0]['user_id'])); 

             if(count( $token) > 0) {
                $tokens_remain  = $token[0]['tokens_remain'];
             } else {
                $tokens_remain  = '0';
             }          

          $data = array(
            'id' => $checkActive[0]['user_id'],
            'firstname' => $checkActive[0]['user_firstname'],
            'lastname' => $checkActive[0]['user_lastname'],
            'gender' => $checkActive[0]['user_gender'],
            'age' => $checkActive[0]['user_age'],
            'age_range_min' => $checkActive[0]['user_age_range_min'],
            'age_range_max' => $checkActive[0]['user_age_range_max'],
            'description' => $checkActive[0]['user_description'],
            'profileimage' => base_url().'uploads/profileimages/'.$checkActive[0]['user_profileimage'],
            'subscription' => $checkActive[0]['user_subscription'],
            'deviceid' => $checkActive[0]['user_deviceid'],
            'token_remain'  => $tokens_remain
          );

          $remainingPing = $this->common_model->select('*', TB_USERPING, array('userping_user_id' => $data['id']) );

          if(count($remainingPing) > 0){
            $pingCount = $remainingPing[0]['userping_remainingCount'];
          }else{
            //get free ping data
            $freePing = $this->common_model->select('*', TB_FREEPING, array());
            $ping_limit = $freePing[0]['ping_limit'];
            // $ping_hr    = $freePing[0]['ping_hr'];

            $data1['userping_user_id']  = $data['id'];
            $data1['userping_pingCount'] = 0;
            $data1['userping_remainingCount'] = $ping_limit;
            $data1['created_at'] = date('Y-m-d H:i:s');
            $res = $this->common_model->insert(TB_USERPING, $data1);
            $pingCount = $ping_limit;
          }

          if($data['subscription'] == 0) {
            $this->response(array('status' => true, 'message' => 'Login successful', 'data' => $data,'edittype' =>'0', 'pingCount' => $pingCount ), 200); 
          } else {

                if($data['subscription'] == 1) {
                  $purchase = $this->common_model->select('*', TB_PURCHASE, array('user_id' => $data['id'])); 

                  $paid = array('start_time'      => $purchase[0]['start_time'], 
                                'end_time'       => $purchase[0]['end_time']
                               ); 
                } else {
                  $purchase = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $data['id'])); 

                  $paid = array('start_time'      => $purchase[0]['tokens_start_time'], 
                                'end_time'       => $purchase[0]['tokens_end_time']
                               );
                } 

            $this->response(array('status' => true, 'message' => 'Login successful', 'data' => $data ,'membership'=> $paid,'edittype' =>'0', 'pingCount' => $pingCount ), 200); 
          }  

        } else {
          $this->response(array('status' => false, 'message' => 'Your Account is Deactive','edittype' =>'3'), 200); 
        }

      } else {
        $this->response(array('status' => false, 'message' => 'New user Go to OTP screen','edittype' =>'2'), 200); 
      }

    }    


    if($this->post('flagtype') == 2 ){
      
      if ($this->post('facebook_id') == "") {
        $this->response(array('status' => false, 'message' => 'Please enter facebook id'), 200);
      }

      if (trim($this->post('mobileno')) == "") {
        $this->response(array('status' => false, 'message' => 'Please enter mobile number'), 200);
      }

      $checkMob = $this->common_model->select('*', TB_USER, array('user_mobileno' => $this->post('mobileno')));

      if (count($checkMob) > 0) {
        if($checkMob[0]['user_facebook_id'] != ''){

          $checkActive = $this->common_model->select('*', TB_USER, array('user_mobileno' => $this->post('mobileno'),'user_status' => '0'));

          if (count($checkActive) > 0) {

               if($checkMob[0]['user_facebook_id'] == $this->post('facebook_id')) {

            $this->common_model->update(TB_USER, array('user_mobileno' => $this->post('mobileno')), array('user_facebook_id' => $this->post('facebook_id'), 'user_deviceid' => $this->post('deviceid') ));


             $token = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $checkActive[0]['user_id'])); 

             if(count( $token) > 0) {
                $tokens_remain  = $token[0]['tokens_remain'];
             } else {
                $tokens_remain  = '0';
             }             

            $data = array(
              'id' => $checkActive[0]['user_id'],
              'firstname' => $checkActive[0]['user_firstname'],
              'lastname' => $checkActive[0]['user_lastname'],
              'gender' => $checkActive[0]['user_gender'],
              'age' => $checkActive[0]['user_age'],
              'age_range_min' => $checkActive[0]['user_age_range_min'],
              'age_range_max' => $checkActive[0]['user_age_range_max'],
              'description' => $checkActive[0]['user_description'],
              'profileimage' => base_url().'uploads/profileimages/'.$checkActive[0]['user_profileimage'],
              'subscription' => $checkActive[0]['user_subscription'],
              'deviceid' => $checkActive[0]['user_deviceid'],
              'token_remain'  => $tokens_remain
            );

            $remainingPing = $this->common_model->select('*', TB_USERPING, array('userping_user_id' => $data['id']) );

            if(count($remainingPing) > 0){
              $pingCount = $remainingPing[0]['userping_remainingCount'];
            }else{
              //get free ping data
              $freePing = $this->common_model->select('*', TB_FREEPING, array());
              $ping_limit = $freePing[0]['ping_limit'];
              // $ping_hr    = $freePing[0]['ping_hr'];

              $data1['userping_user_id']  = $data['id'];
              $data1['userping_pingCount'] = 0;
              $data1['userping_remainingCount'] = $ping_limit;
              $data1['created_at'] = date('Y-m-d H:i:s');
              $res = $this->common_model->insert(TB_USERPING, $data1);
              $pingCount = $ping_limit;
            }

            if($data['subscription'] == 0) {
              $this->response(array('status' => true, 'message' => 'Login successful', 'data' => $data,'edittype' =>'0', 'pingCount' => $pingCount ), 200); 
            } else {

                if($data['subscription'] == 1) {
                  $purchase = $this->common_model->select('*', TB_PURCHASE, array('user_id' => $data['id'])); 

                  $paid = array('start_time'      => $purchase[0]['start_time'], 
                                'end_time'       => $purchase[0]['end_time']
                               ); 
                } else {
                  $purchase = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $data['id'])); 

                  $paid = array('start_time'      => $purchase[0]['tokens_start_time'], 
                                'end_time'       => $purchase[0]['tokens_end_time']
                               );
                } 

                $this->response(array('status' => true, 'message' => 'Login successful', 'data' => $data ,'membership'=> $paid,'edittype' =>'0', 'pingCount' => $pingCount ), 200); 
            } 

          } else {
             $this->response(array('status' => false, 'message' => 'Mobile Already Exists','edittype' =>'3'), 200);  
          }  

          }else{
            $this->response(array('status' => false, 'message' => 'Your Account is Deactive','edittype' =>'3'), 200); 
          }            
          
        }else{
          $this->response(array('status' => false, 'message' => 'Mobile Already Exists','edittype' =>'3'), 200);
        }
      

      }else{
        $this->response(array('status' => false, 'message' => 'New user Go to OTP screen','edittype' =>'1'), 200);
      }

    }
  }

  /**
   * My profile
   * @param type $user_id  => to get profile of map users
   */
  public function pingProfile_post() {

    if ($this->post('user_id') < 1) {
        $this->response(array('status' => false, 'message' => 'Please enter user id'), 200);
    }
    // if ($this->post('login_user_id') < 1) {
    //     $this->response(array('status' => false, 'message' => 'Please enter login user id'), 200);
    // }
    
    $pingCheck = $this->common_model->select('*', TB_PING, array('sender_id' => $this->post('user_id'), 'receiver_id' => $this->post('login_user_id'), 'ping_accept_status' => 0 ));

      if(count($pingCheck) > 0) {

        $getNotData = $this->common_model->select('*', TB_NOTDATA, array('notdata_detail_id' => $pingCheck[0]['ping_id'], 'notdata_type' => 'ping send' ));

            if(count($getNotData) > 0) {

              $this->common_model->update(TB_NOTDATA, array('notdata_id' => $getNotData[0]['notdata_id']), array('notdata_read_status' => 1));
            } 
      }

          $userArr = $this->common_model->select('*',TB_USER, array('user_id' => $this->post('user_id')));
         
          if (count($userArr) > 0) {

                if($userArr[0]['user_profileimage'] != ''){
                $userArr[0]['user_profileimage'] = base_url().'uploads/profileimages/'.$userArr[0]['user_profileimage'];
                }
                if($userArr[0]['user_image1'] != ''){
                  $userArr[0]['user_image1'] = base_url().'uploads/profileimages/'.$userArr[0]['user_image1'];
                }
                if($userArr[0]['user_image2'] != ''){
                  $userArr[0]['user_image2'] = base_url().'uploads/profileimages/'.$userArr[0]['user_image2'];
                }
                if($userArr[0]['user_image3'] != ''){
                  $userArr[0]['user_image3'] = base_url().'uploads/profileimages/'.$userArr[0]['user_image3'];
                }
            $this->response(array('status' => true, 'message' => 'check the profile', 'data' => $userArr), 200);
          } else {
            $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
          }
  }  


  
  public function pingProfileMap_post() {

    if ($this->post('senderUser_id') < 1) {
      $this->response(array('status' => false, 'message' => 'Please enter sender userid'), 200);
    }
    if ($this->post('profileUser_id') < 1) {
      $this->response(array('status' => false, 'message' => 'Please enter profile userid'), 200);
    }   

    $userExists = $this->common_model->select('*', TB_USER, array('user_id' => $this->post('senderUser_id')));

    if(count($userExists) > 0) {

      $senderUser_id = $this->post('senderUser_id');
      $profileUser_id = $this->post('profileUser_id');

      $pingExists = $this->common_model->getUserPingExists($senderUser_id,$profileUser_id);
      
      if(count($pingExists) > 0) {

        if($pingExists[0]['ping_accept_status'] == 0) {
          $pingStatus = 1;
          $ping_start_time = $pingExists[0]['ping_receive_at'];
        } else {
          $pingStatus = 2;
          $ping_start_time = '';
        }
      } else {
        $pingStatus = 0;
        $ping_start_time = '';
      }

      $userArr = $this->common_model->select('*', TB_USER, array('user_id' => $this->post('profileUser_id'), 'user_loginstatus' => 1));
      // echo $this->db->last_query();
      if (count($userArr) > 0) {

        if($userArr[0]['user_profileimage'] != ''){
          $userArr[0]['user_profileimage'] = base_url().'uploads/profileimages/'.$userArr[0]['user_profileimage'];
        }
        if($userArr[0]['user_image1'] != ''){
          $userArr[0]['user_image1'] = base_url().'uploads/profileimages/'.$userArr[0]['user_image1'];
        }
        if($userArr[0]['user_image2'] != ''){
          $userArr[0]['user_image2'] = base_url().'uploads/profileimages/'.$userArr[0]['user_image2'];
        }
        if($userArr[0]['user_image3'] != ''){
          $userArr[0]['user_image3'] = base_url().'uploads/profileimages/'.$userArr[0]['user_image3'];
        }

        $userArr[0]['pingStatus'] = $pingStatus;
        $userArr[0]['pingStartTime'] = $ping_start_time;
        $this->response(array('status' => true, 'message' => 'check the profile', 'data' => $userArr), 200);
      } else {
        $this->response(array('status' => false, 'message' => 'error'), 200);
      }

    } else {
      $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }    
  }  


    /**
     * Edit My profile
     * @param type $user_id  => to get profile of map users
     */
  public function editProfile_post() {

    $userExists = $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));  
    if(count($userExists) > 0) {  

      $name  = $this->post('name');
      $pieces = explode(" ", $name);
      if(count($pieces) >1){
        $firstname = $pieces[0];
        $lastname = $pieces[1];
      } else {
        $firstname = $pieces[0];
        $lastname = '';  
      }

      $range = $this->post('age_range');
      $myArray = explode("-", $range);

      $userdata = array(
        'user_firstname' => $firstname,
        'user_lastname' => $lastname,
        'user_age' => $this->post('age'),
        'user_gender' => $this->post('gender'),
        'user_age_range_min' => $myArray[0],
        'user_age_range_max' => $myArray[1],
        'user_description' => $this->post('description')
      ); 

      if (isset($_FILES['user_profileimage']) && !empty($_FILES['user_profileimage'])) {
        $uploaddir = './uploads/profileimages/';
        $file_name = underscore($_FILES['user_profileimage']['name']);
        $path = $_FILES['user_profileimage']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $user_img = rand() . '_profile.' . $ext;
        $uploadfile = $uploaddir . $user_img;

        move_uploaded_file($_FILES['user_profileimage']['tmp_name'], $uploadfile);
        $userdata['user_profileimage'] = $user_img;
      }

      if (isset($_FILES['user_image1']) && !empty($_FILES['user_image1'])) {
        $uploaddir = './uploads/profileimages/';
        $file_name = underscore($_FILES['user_image1']['name']);
        $path = $_FILES['user_image1']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $user_img1 = rand() . '_image1.' . $ext;
        $uploadfile1 = $uploaddir . $user_img1;

        move_uploaded_file($_FILES['user_image1']['tmp_name'], $uploadfile1);
        $userdata['user_image1'] = $user_img1;  
      } 
      
      if (isset($_FILES['user_image2']) && !empty($_FILES['user_image2'])) {
        $uploaddir = './uploads/profileimages/';
        $file_name = underscore($_FILES['user_image2']['name']);
        $path = $_FILES['user_image2']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $user_img2 = rand() . '_image2.' . $ext;
        $uploadfile2 = $uploaddir . $user_img2;

        move_uploaded_file($_FILES['user_image2']['tmp_name'], $uploadfile2);
        $userdata['user_image2'] = $user_img2;  
      } 

      if (isset($_FILES['user_image3']) && !empty($_FILES['user_image3'])) {
        $uploaddir = './uploads/profileimages/';
        $file_name = underscore($_FILES['user_image3']['name']);
        $path = $_FILES['user_image3']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $user_img3 = rand() . '_image3.' . $ext;
        $uploadfile3 = $uploaddir . $user_img3;
        move_uploaded_file($_FILES['user_image3']['tmp_name'], $uploadfile3);
        $userdata['user_image3'] = $user_img3;  
      } 

      $result = $this->common_model->update(TB_USER, array('user_id' => $this->post('user_id')), $userdata);
      
      $userArr = $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id'), 'user_loginstatus' => 1));
      $datas = array('id' => $userArr[0]['user_id'],
                      'firstname' => $userArr[0]['user_firstname'],
                      'lastname' => $userArr[0]['user_lastname'],
                      'mobileno' => $userArr[0]['user_mobileno'],
                      'gender' => $userArr[0]['user_gender'],
                      'age' => $userArr[0]['user_age'],
                      'age_range_min' => $userArr[0]['user_age_range_min'],
                      'age_range_max' => $userArr[0]['user_age_range_max'],
                      'description' => $userArr[0]['user_description'],
                      'profileimage' => base_url().'uploads/profileimages/'.$userArr[0]['user_profileimage'],
                      'image1' => base_url().'uploads/profileimages/'.$userArr[0]['user_image1'],
                      'image2' => base_url().'uploads/profileimages/'.$userArr[0]['user_image2'],
                      'image3' => base_url().'uploads/profileimages/'.$userArr[0]['user_image3'],
                      'subscription' => $userArr[0]['user_subscription'],
                      'deviceid' => $userArr[0]['user_deviceid']
                    ); 

      $remainingPing = $this->common_model->select('*', TB_USERPING, array('userping_user_id' => $datas['id']) );

      if(count($remainingPing) > 0){
        $pingCount = $remainingPing[0]['userping_remainingCount'];
      }else{
        //get free ping data
        $freePing = $this->common_model->select('*', TB_FREEPING, array());
        $ping_limit = $freePing[0]['ping_limit'];
        // $ping_hr    = $freePing[0]['ping_hr'];

        $data1['userping_user_id']  = $datas['id'];
        $data1['userping_pingCount'] = 0;
        $data1['userping_remainingCount'] = $ping_limit;
        $data1['created_at'] = $this->post('created_at');
        $res = $this->common_model->insert(TB_USERPING, $data1);
        $pingCount = $ping_limit;
      }    

      if($result) {
        $this->response(array('status' => true, 'message' => 'User Profile Updated Sucessfully', 'data' => $datas, 'pingCount' => $pingCount ), 200);
      } else {
        $this->response(array('status' => false, 'message' => 'Failed to update profile'), 200);
      } 

    } else {
        $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }  

  }  



    public function updateMobileNumber_post() {

      if (trim($this->post('user_id')) == "") {
        $this->response(array('status' => false, 'message' => 'Enter User id'), 200);
      }
      if (trim($this->post('new_mobileno')) == "") {
        $this->response(array('status' => false, 'message' => 'Please enter new mobile number'), 200);
      }
      if (trim($this->post('deviceid')) == "") {
        $this->response(array('status' => false, 'message' => 'Please enter deviceid'), 200);
      }

      $userDevice = $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));

      if(count($userDevice) > 0) {

        $checkMob =  $this->common_model->checkMobileExists($this->post('new_mobileno'),$this->post('user_id'));

        if(count($checkMob) > 0) {

            $this->response(array('status' => true, 'message' => 'Mobile Number already exists', 'loginflag' => 0), 200);

        } else {

            $this->common_model->update(TB_USER, array('user_id' => $this->post('user_id')), array('user_mobileno' => $this->post('new_mobileno'),'user_deviceid' => $this->post('deviceid'), 'user_verified' =>1)); 

            $this->response(array('status' => true, 'message' => 'Mobile Number has been updated', 'loginflag' => 1), 200);

        }     
        
      } else {

        $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
      }
    } 



// fetch current location

    public function currentlocation_post() {
        
      $user_id = $this->post('user_id');

      if (trim($this->post('lat')) == "") {
        $this->response(array('status' => false, 'message' => 'Network error'), 200);
      }
      if (trim($this->post('long')) == "") {
        $this->response(array('status' => false, 'message' => 'Network error'), 200);
      }   

      $userlogin= $this->common_model->select('user_id', TB_USER, array('user_id' => $user_id,'user_loginstatus' => 1));
      //print_r($userlogin);
           
      if(count($userlogin) > 0) {

        $userArr = $this->common_model->select('*', TB_USER, array('user_id' => $user_id));

        if (count($userArr) > 0) {

          $location['user_lat'] = $this->post('lat');
          $location['user_long'] = $this->post('long');

          $this->common_model->update(TB_USER, array('user_id' => $user_id), $location);

          $range =  $this->common_model->select('range_from,range_to', TB_RANGE, array());
          $Range   = $range[0]['range_to'];
          $miles   = 0.00018939*$Range;
          //$miles ='6';

          $subscription = $this->common_model->select('user_subscription', TB_USER, array('user_id' => $user_id));
          // print_r($subscription);
          $result = $subscription[0]['user_subscription'];

          if($result == '0') {

            $status = $this->common_model->select('user_onlinestatus', TB_USER, array('user_id' => $user_id));
            // print_r($status);
            $onlineStatus = $status[0]['user_onlinestatus'];

            if($onlineStatus == '1') {

              $currentUserLoc = $this->common_model->select('user_lat,user_long, user_age_range_min, user_age_range_max, user_interest', TB_USER, array('user_id' => $user_id));
              $lat = $currentUserLoc[0]['user_lat'];
              $long = $currentUserLoc[0]['user_long'];
              $range_min = $currentUserLoc[0]['user_age_range_min'];
              $range_max = $currentUserLoc[0]['user_age_range_max'];
              $search = $currentUserLoc[0]['user_interest'];
              $subscription = '0';

              if($search == 'Both') {

                $users = $this->common_model->usersByRangeOnline($lat, $long, $miles,$user_id,$subscription,$range_min,$range_max);

                for($i = 0; $i < count($users); $i++) {

                  if($users[$i]['profileimage'] != ''){
                    $users[$i]['profileimage'] = base_url().'uploads/profileimages/'.$users[$i]['profileimage'];
                  }
                  
                  $pingExists = $this->common_model->getUserPingExists($user_id,$users[$i]['user_id']);

                  if(count($pingExists) > 0) {  
                      if($pingExists[0]['sender_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 0) {
                        $users[$i]['pingFlag'] = "1";
                      } else if($pingExists[0]['sender_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 2){
                        $users[$i]['pingFlag'] = "2";
                      } else if($pingExists[0]['receiver_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 0) {
                        $users[$i]['pingFlag'] = "6";
                      } else{
                        $users[$i]['pingFlag'] = "0";
                      }

                  } else {
                     $users[$i]['pingFlag'] = "0";
                  }  
              }

              
              } else {

                $users = $this->common_model->usersByRangeDataOnline($lat, $long, $miles,$user_id,$subscription,$range_min,$range_max,$search);

                for($j = 0; $j < count($users); $j++) {
                  if($users[$j]['profileimage'] != ''){
                    $users[$j]['profileimage'] = base_url().'uploads/profileimages/'.$users[$j]['profileimage'];
                  }

                  $pingExists = $this->common_model->getUserPingExists($user_id,$users[$j]['user_id']);

                  if(count($pingExists) > 0) { 

                    if($pingExists[0]['sender_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 0) {
                      $users[$j]['pingFlag'] = "1";
                    } else if($pingExists[0]['sender_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 2){
                      $users[$j]['pingFlag'] = "2";
                    } else if($pingExists[0]['receiver_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 0) {
                      $users[$j]['pingFlag'] = "6";
                    } else{
                        $users[$j]['pingFlag'] = "0";
                    }

                  } else {
                     $users[$j]['pingFlag'] = "0";
                  }                   
                }
              }

              $this->response(array('status' => true,'message' => 'online users','data' =>$users), 200);

            } else {

              $this->response(array('status' => true,'message' => 'You are in offline mode'), 200); 
            }
                        
          } else {
                           
            $currentUserLoc = $this->common_model->select('user_lat,user_long, user_age_range_min, user_age_range_max, user_interest', TB_USER, array('user_id' => $user_id));
            // print_r($currentUserLoc);

            $lat = $currentUserLoc[0]['user_lat'];
            $long = $currentUserLoc[0]['user_long'];
            $range_min = $currentUserLoc[0]['user_age_range_min'];
            $range_max = $currentUserLoc[0]['user_age_range_max'];
            $search = $currentUserLoc[0]['user_interest'];
            //$subscription = '1';
            
            if($search == 'Both') {

              $users = $this->common_model->usersByRangeOffline($lat, $long, $miles,$user_id,$range_min,$range_max);
              
            for($k = 0; $k < count($users); $k++) {
              if($users[$k]['profileimage'] != ''){
                $users[$k]['profileimage'] = base_url().'uploads/profileimages/'.$users[$k]['profileimage'];
              }

                $pingExists = $this->common_model->getUserPingExists($user_id,$users[$k]['user_id']);
                if(count($pingExists) > 0) { 

                  if($pingExists[0]['sender_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 0) {
                    $users[$k]['pingFlag'] = "1";
                  } else if($pingExists[0]['sender_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 2){
                    $users[$k]['pingFlag'] = "2";
                  } else if($pingExists[0]['receiver_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 0) {
                    $users[$k]['pingFlag'] = "6";
                  } else{
                        $users[$k]['pingFlag'] = "0";
                      }
                } else {
                     $users[$k]['pingFlag'] = "0";
                }                

            }

          } else {
            
            $users = $this->common_model->usersByRangeDataOffline($lat, $long, $miles,$user_id,$range_min,$range_max,$search);


            for($l = 0; $l < count($users); $l++) {
              if($users[$l]['profileimage'] != ''){
                $users[$l]['profileimage'] = base_url().'uploads/profileimages/'.$users[$l]['profileimage'];
              }

                $pingExists = $this->common_model->getUserPingExists($user_id,$users[$l]['user_id']);

                if(count($pingExists) > 0) { 

                  if($pingExists[0]['sender_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 0) {
                    $users[$l]['pingFlag'] = "1";
                  } else if($pingExists[0]['sender_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 2){
                    $users[$l]['pingFlag'] = "2";
                  } else if($pingExists[0]['receiver_id'] == $user_id && $pingExists[0]['ping_accept_status'] == 0) {
                    $users[$l]['pingFlag'] = "6";
                  } else{
                        $users[$l]['pingFlag'] = "0";
                      }

                } else {
                     $users[$l]['pingFlag'] = "0";
                }              
            }
          }
          
          $this->response(array('status' => true,'message' => 'display users for paid user','data'=>$users), 200); 
        } 

      } else {

        $this->response(array('status' => false, 'message' => 'user is not using app'), 200);
      } 
            
    } else {
      $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }
  }

// SEARCH USERS ON MAP BY NAME

  public function searchUser_post() {
     
    if (trim($this->post('name')) == "") {
      $this->response(array('status' => false, 'message' => 'Enter name you want to search'), 200);
    }

    $userExists= $this->common_model->select('user_id', TB_USER, array('user_id' => $this->post('user_id'),'user_loginstatus' => 1));
  
    if(count($userExists) > 0) {  

      $search  = $this->post('name');
      $user_id = $this->post('user_id');

      $range =  $this->common_model->select('range_from,range_to', TB_RANGE, array());
      $Range   = $range[0]['range_to'];
      $miles   = 0.00018939*$Range;

      $currentUserLoc = $this->common_model->select('user_lat,user_long,user_interest,user_age_range_min,user_age_range_max', TB_USER, array('user_id' => $this->post('user_id')));

      if(count($currentUserLoc) > 0) {

        $lat = $currentUserLoc[0]['user_lat'];
        $long = $currentUserLoc[0]['user_long'];
        $searchPre = $currentUserLoc[0]['user_interest'];
        $range_min = $currentUserLoc[0]['user_age_range_min'];
        $range_max = $currentUserLoc[0]['user_age_range_max']; 

        if($searchPre == 'Both') {

            $userArr = $this->common_model->searchUserByName($lat, $long, $miles,$search,$user_id,$range_min,$range_max);
        } else {
            $userArr = $this->common_model->searchUserByNamePre($lat, $long, $miles,$search,$user_id,$searchPre,$range_min,$range_max);
        }

        
        
        for($i = 0; $i < count($userArr); $i++) {

          $userPing = $this->common_model->getUserPingExists($user_id,$userArr[$i]['user_id']);
          

          if($userArr[$i]['user_profileimage'] != ''){
            $userArr[$i]['user_profileimage'] = base_url().'uploads/profileimages/'.$userArr[$i]['user_profileimage'];
          }
          if($userArr[$i]['user_image1'] != ''){
            $userArr[$i]['user_image1'] = base_url().'uploads/profileimages/'.$userArr[$i]['user_image1'];
          }
          if($userArr[$i]['user_image2'] != ''){
            $userArr[$i]['user_image2'] = base_url().'uploads/profileimages/'.$userArr[$i]['user_image2'];
          }
          if($userArr[$i]['user_image3'] != ''){
            $userArr[$i]['user_image3'] = base_url().'uploads/profileimages/'.$userArr[$i]['user_image3'];
          }
          if(count($userPing) > 0) {
            if(isset($userPing[$i]['ping_accept_status']) == 0) {
              $userArr[$i]['searchFlag'] = '1';
            } else {
              $userArr[$i]['searchFlag'] = '2';
            }
          } else {
             $userArr[$i]['searchFlag'] = '0';
          }
          
        }

        if(count($userArr) > 0) {
          $this->response(array('status' => true,'message' => 'search result','data'=>$userArr), 200);
        } else {
          $this->response(array('status' => true,'message' => 'Not found'), 200);
        } 
      } else {
        $this->response(array('status' => true,'message' => 'User Not Present'), 200);
      }  

    } else {
      $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }
    
  } 

// PREFERENCES SEARCH BY 

  public function preferenceSearch_post() {

    if (trim($this->post('user_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Please enter user id'), 200);
    }
    if (trim($this->post('searchtype')) == "") {
      $this->response(array('status' => false, 'message' => 'Please enter the search type data'), 200);
    }

  $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
  
  if(count($userExists) > 0) {  

      $searchtype   = $this->post('searchtype');
      $user_id  = $this->post('user_id');

      if($searchtype == 1) {             // for search only female users

        $search   = 'Female'; 

        $result = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_interest' => $search)); 

        if($result) {
          $this->response(array('status' => true, 'message' => 'Preference has been updated'), 200);
        } else {
          $this->response(array('status' => true, 'message' => 'Fail to updated'), 200);
        }           
      } 

    if($searchtype == 2) {             // for search only male users

      $search   = 'Male'; 
      $result = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_interest' => $search)); 

      if($result) {
        $this->response(array('status' => true, 'message' => 'Preference has been updated'), 200);
      } else {
        $this->response(array('status' => true, 'message' => 'Fail to updated'), 200);
      }                     
    } 

    if($searchtype == 3) {             // for search users by age range

      $range_min  = $this->post('range_min');
      $range_max  = $this->post('range_max');

      $result = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_age_range_min' => $range_min, 'user_age_range_max' => $range_max)); 

      if($result) {
        $this->response(array('status' => true, 'message' => 'Age range has been updated'), 200);
      } else {
        $this->response(array('status' => true, 'message' => 'Fail to updated'), 200);
      }                   
    } 

    if($searchtype == 4) {             // for search users by age range with only female users

      $range_min  = $this->post('range_min');
      $range_max  = $this->post('range_max');
      $search     = 'Female'; 

      $result = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_age_range_min' => $range_min, 'user_age_range_max' => $range_max, 'user_interest' => $search)); 

      if($result) {
        $this->response(array('status' => true, 'message' => 'Age range and preference has been updated'), 200);
      } else {
        $this->response(array('status' => true, 'message' => 'Fail to updated'), 200);
      }                       
    }       

    if($searchtype == 5) {             // for search users by age range with only male users

      $range_min  = $this->post('range_min');
      $range_max  = $this->post('range_max');
      $search     = 'Male'; 

      $result = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_age_range_min' => $range_min, 'user_age_range_max' => $range_max, 'user_interest' => $search)); 

      if($result) {
        $this->response(array('status' => true, 'message' => 'Age range and preference has been updated'), 200);
      } else {
        $this->response(array('status' => true, 'message' => 'Fail to updated'), 200);
      }                    
    }    

    if($searchtype == 6) {             // for search users female and male users
      $search     = 'Both'; 

      $result = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_interest' => $search)); 

      if($result) {
        $this->response(array('status' => true, 'message' => 'Preference has been updated'), 200);
      } else {
        $this->response(array('status' => true, 'message' => 'Fail to updated'), 200);
      }                    
    } 

    if($searchtype == 7) {             // for search users by age range with female and male users

      $range_min  = $this->post('range_min');
      $range_max  = $this->post('range_max');
      $search     = 'Both'; 

      $result = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_age_range_min' => $range_min, 'user_age_range_max' => $range_max, 'user_interest' => $search)); 

      if($result) {
        $this->response(array('status' => true, 'message' => 'Age range and preference has been updated'), 200);
      } else {
        $this->response(array('status' => true, 'message' => 'Fail to updated'), 200);
      }                    
    }

    } else {
       $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    } 

  }


  public function preferenceData_post() {

    if (trim($this->post('user_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
    }

  $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
  if(count($userExists) > 0) {      

        $prefdata = $this->common_model->select('user_age_range_min , user_age_range_max , user_interest', TB_USER, array('user_id' => $this->post('user_id')));

        $datas = array( 
          'age_range_min'   => $prefdata[0]['user_age_range_min'],
          'age_range_max'   => $prefdata[0]['user_age_range_max'],
          'user_preference' => $prefdata[0]['user_interest']
        );  
        $this->response(array('status' => true, 'message' => 'show data', 'data' => $datas), 200);

    } else {
       $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }     

  }


/* Chatting between two users */    


  public function chatting_post() {
        
    if (trim($this->post('sender_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Sender is missing'), 200);
    }
    if (trim($this->post('receiver_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Receiver is missing'), 200);
    }

  $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('sender_id')));
  if(count($userExists) > 0) { 

    
    $flag        = $this->post('status_flag');
    $sender_id   = $this->post('sender_id');
    $receiver_id = $this->post('receiver_id');

      $pingCheck = $this->common_model->select('*', TB_PING, array('sender_id' => $this->post('receiver_id'), 'receiver_id' => $this->post('sender_id'), 'ping_accept_status' => 2 ));

        if(count($pingCheck) > 0) {

          $getNotData = $this->common_model->select('*', TB_NOTDATA, array('notdata_detail_id' => $pingCheck[0]['ping_id'], 'notdata_type' => 'ping accept' ));

              if(count($getNotData) > 0) {

                $this->common_model->update(TB_NOTDATA, array('notdata_id' => $getNotData[0]['notdata_id']), array('notdata_read_status' => 1));

                  $chatMsgCheck = $this->common_model->select('*', TB_CHAT, array('sender_id' => $receiver_id, 'receiver_id' => $sender_id));

                    if(count($chatMsgCheck) > 0) {
                      
                        $this->common_model->update(TB_CHAT, array('sender_id' => $receiver_id,'receiver_id' => $sender_id), array('msg_status' => 1));
                    }
              } 
        }
   

    //DELETE CHAT BEFORE 15 MIN
    $dataDelete = $this->common_model->select('*', TB_CHAT, array('sender_id' => $sender_id, 'receiver_id' => $receiver_id));
    if(count($dataDelete) > 0) {

      $before15mins = strtotime('-15 minutes');
      $newtime = date('Y-m-d H:i:s', $before15mins);
      $deleteChatMsg = $this->common_model->chatMsgDelete($newtime);
    }

    if($this->post('message') != '') {
       

      $chat = array(
        'sender_id'   => $sender_id,
        'receiver_id' => $receiver_id,
        'chat_msg'    => $this->post('message'),
        'created_at'  => date('Y-m-d H:i:s'),
        'msg_status'  => '0'
      );  
      $result = $this->common_model->insert(TB_CHAT, $chat);

     if($result) {

      $notify = $this->common_model->select('*', TB_USER, array('user_id' => $receiver_id));
      $sender = $this->common_model->select('*', TB_USER, array('user_id' => $sender_id));

      if($sender[0]['user_deviceid'] == $notify[0]['user_deviceid']) {

              $data = $this->common_model->selectChat($sender_id,$receiver_id);
              if(count($data) > 0) {
                for($i = 0; $i < count($data); $i++ ){
                  
                  $created_at  = strtotime ( '-30 minutes' , strtotime ( $data[$i]['created_at'] ) );
                  $data[$i]['created_at'] = date ( 'Y:m:d H:i:s' , $created_at);

                  if($sender_id == $data[$i]['sender_id']){
                    $data[$i]['sides'] = 'R';                  
                  }else{
                    $data[$i]['sides'] = 'L'; 
                  }
                  if($sender_id == $data[$i]['chat_save_by']){
                    $data[$i]['shows'] = 'Y';  
                  }else{
                    $data[$i]['shows'] = '';  
                  }

                }

                $this->response(array('status' => true, 'message' =>'chatting data', 'data' => $data, 'msg' => 1), 200);    
              }  else {
                $this->response(array('status' => true, 'message' =>'Not Found', 'msg' => 0), 200);
              }

      } else {
   
              $firstname = $sender[0]['user_firstname']; 
              $last  = $sender[0]['user_firstname'];
                  if($last != '') {
                      $lastname = substr($last, 0, 1);
                      $name = $firstname.ucfirst($lastname);
                  } else {
                      $name = $firstname;
                  }

              $notdata = array('notdata_user_id'=> $notify[0]['user_id'],
                               'notdata_device_token' =>$notify[0]['user_deviceid'],
                               'notdata_type' => 'chat'
                                );
              $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);  

              $badgeRows = $this->common_model->select('*', TB_NOTDATA, array('notdata_user_id' => $notify[0]['user_id'], 'notdata_read_status' => 0));

              $badge = count($badgeRows);   
               
              $devicetoken = $notify[0]['user_deviceid'];
              $message     = $name . ' sent a new message';
              $this->mobilePushNotificationChat($devicetoken, $message, 3, $sender_id,$name,$badge,$resnot);

            $data = $this->common_model->selectChat($sender_id,$receiver_id);
            for($i = 0; $i < count($data); $i++ ){
                
                $created_at  = strtotime ( '-30 minutes' , strtotime ( $data[$i]['created_at'] ) );
                $data[$i]['created_at'] = date ( 'Y:m:d H:i:s' , $created_at);

              if($sender_id == $data[$i]['sender_id']){
                $data[$i]['sides'] = 'R'; 
              }else{
                $data[$i]['sides'] = 'L'; 
              }
              if($sender_id == $data[$i]['chat_save_by']){
                $data[$i]['shows'] = 'Y';  
              }else{
                $data[$i]['shows'] = '';  
              }
            }
            $this->response(array('status' => true, 'message' =>'chatting data', 'data' => $data, 'msg' => 1), 200);

      }  

      } else {
        $this->response(array('status' => false,'message' => 'Fail to insert'), 200);
      }

    } else {

      $data = $this->common_model->selectChat($sender_id,$receiver_id);
      if(count($data) > 0) {
        for($i = 0; $i < count($data); $i++ ){
          
          $created_at  = strtotime ( '-30 minutes' , strtotime ( $data[$i]['created_at'] ) );
          $data[$i]['created_at'] = date ( 'Y:m:d H:i:s' , $created_at);

          if($sender_id == $data[$i]['sender_id']){
            $data[$i]['sides'] = 'R';                  
          }else{
            $data[$i]['sides'] = 'L'; 
          }
          if($sender_id == $data[$i]['chat_save_by']){
            $data[$i]['shows'] = 'Y';  
          }else{
            $data[$i]['shows'] = '';  
          }

        }

        $this->response(array('status' => true, 'message' =>'chatting data', 'data' => $data, 'msg' => 1), 200);    
      }  else {
        $this->response(array('status' => true, 'message' =>'Not Found', 'msg' => 0), 200);
      }
    }

    } else {
       $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }    
} 

// Notofication for chatting message

  public  function mobilePushNotificationChat($devicetoken, $message, $id, $sender,$name,$badge,$resnot) {
    $passphrase = '1234etpl';
      
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushcert.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

    $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    $body['aps'] = array(
      'alert' => array(
        'body' => $message,
        'action-loc-key' => 'BondApp',        
      ),
      'status' => $id,
      'sender_id' => $sender,
      'name'    => $name,
      'badge' => $badge,
      'notification_read' => $resnot,
      'sound' => 'oven.caf',
    );
    // Encode the payload as JSON
    $payload = json_encode($body);
    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $devicetoken) . pack('n', strlen($payload)) . $payload;
    $result = fwrite($fp, $msg, strlen($msg));
    //socket_close($fp);
    fclose($fp);
    return $result;
}   

  // Save messages

  public function saveChatMessage_post() {

    $chat_save_by = $this->post('user_id');
    $chat_id      = $this->post('chat_id');

  $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $chat_save_by));
  if(count($userExists) > 0) {          

        $chats = explode(",",$chat_id);
         for($i=0; $i < count($chats); $i++) {
          $result = $this->db->query('UPDATE `tbl_chat` SET `msg_status` = 1 , `chat_save_by` = "'.$chat_save_by.'" WHERE `chat_id`  = "'.$chats[$i].'" ');
        }

        if($result) {
          $this->response(array('status' => true,'message' => 'save chat'), 200);
        } else {
          $this->response(array('status' => false,'message' => 'Fail to save'), 200);
        } 

  } else {
    $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
  }          

  }



// REQUEST TO ADMIN 
   public function requestAdmin_post() {

    $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
    if(count($userExists) > 0) {          
       
        if (trim($this->post('type')) == "") {
          $this->response(array('status' => false, 'message' => 'select your request type'), 200);
        }
        if (trim($this->post('name')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter your name'), 200);
        }
        if (trim($this->post('email')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter email id'), 200);
        }
        if (trim($this->post('msg')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter description'), 200);
        }   
        $request['user_id']      = $this->post('user_id');
        $request['request_type'] = $this->post('type');
        $request['name']         = $this->post('name');
        $request['email']        = $this->post('email');
        $request['message']      = $this->post('msg');

        $this->common_model->insert(TB_REQUEST, $request);

        $this->response(array('status' => true,'message' => 'Request send successfully'), 200);
    } else {
    $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }     

  } 


/*  TO CHANGE STATUS EITHER ONLINE =0 OR OFFLINE =1 */

  public function onlineStatus_post() {

    $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
    if(count($userExists) > 0) {            
         
        $this->common_model->update(TB_USER, array('user_id' => $this->post('user_id')), array('user_onlinestatus' => $this->post('user_onlinestatus')));

        $this->response(array('status' => true,'message' => 'status update'), 200);
    } else {
    $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }

  } 


 /* Send request by notification   */ 

public function pingRquest_post() {

 $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_sender_id')));
  if(count($userExists) > 0) {       
     
    if (trim($this->post('user_sender_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Sender id missing'), 200);
    }
    if (trim($this->post('user_receiver_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Receiver id missing'), 200);
    }
    if (trim($this->post('ping_flag')) == "") {
      $this->response(array('status' => false, 'message' => 'flag is missing'), 200);
    } 
    if (trim($this->post('created_at')) == "") {
      $this->response(array('status' => false, 'message' => 'time is missing'), 200);
    } 

    //get free ping data
    $freePing = $this->common_model->getFreePingsData();

    $ping_type  = $freePing[0]['ping_type'];
    $ping_limit = $freePing[0]['ping_limit'];
    $ping_hr    = $freePing[0]['ping_hr'];
    $ping_start    = $freePing[0]['startdate'];
    $ping_end    = $freePing[0]['enddate'];

    $flag     = $this->post('ping_flag');
    $sender   = $this->post('user_sender_id');
    $receiver = $this->post('user_receiver_id');
    $created_at = $this->post('created_at');

    $userReceiver = $this->common_model->select('*', TB_USER, array('user_id' => $receiver));
    
    $userSender = $this->common_model->select('*', TB_USER, array('user_id' => $sender)); 
    

    if(count($userReceiver) && count($userSender) > 0) {

        if($userReceiver[0]['user_lastname'] != '') {
            $firstname = $userReceiver[0]['user_firstname'];
            $lastname  = substr($userReceiver[0]['user_lastname'] ,0 ,1);
            $namereceiver =  $firstname.' '.$lastname;
        } else {
            $firstname = $userReceiver[0]['user_firstname'];
            $namereceiver =  $firstname;
        }

        if($userSender[0]['user_lastname'] != '') {
            $firstname = $userSender[0]['user_firstname'];
            $lastname  = substr($userSender[0]['user_lastname'] ,0 ,1);
            $namesender =  $firstname.' '.$lastname;
        } else {
            $firstname = $userSender[0]['user_firstname'];
            $namesender =  $firstname;
        }        
          
    // USER ALREADY EXISTS IN PING TABLE


            
  if($flag == '1') {   // 1 status for send ping

      $checkExists = $this->common_model->getUserPingExists($sender,$receiver);


    if(count($checkExists) == 0) {

        // check user have membership or not
        $checkSubscription = $this->common_model->Select('*',TB_USER, array('user_id' => $sender)); 
        $subscription = $checkSubscription[0]['user_subscription'];

      if($subscription == 0) {
         $current = date('Y-m-d'); 
         
        if($ping_start < $current && $ping_end > $current) {  // when ping type is unlimited

          if($userReceiver[0]['user_loginstatus'] == 1) {

              $send_at   = $created_at; 
              
              $data12 = array('sender_id'=> $sender,
                              'receiver_id' => $receiver,
                              'ping_send_status' => '1', // send the ping
                              'ping_receive_at'  => $send_at,
                              'ping_update_at'  => $send_at
                            );
              $res = $this->common_model->insert(TB_PING, $data12);

              $notdata = array('notdata_user_id'=> $receiver,
                               'notdata_device_token' =>$userReceiver[0]['user_deviceid'],
                               'notdata_type' => 'ping send',
                               'notdata_detail_id' => $res
                            );
              $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

              $badgeRows = $this->common_model->select('*', TB_NOTDATA, array('notdata_user_id' => $userReceiver[0]['user_id'], 'notdata_read_status' => 0));

              $badge = count($badgeRows);    

              $devicetoken = $userReceiver[0]['user_deviceid'];
              $message = $namesender .' '.'Pinged you, check it now!';
              $notification = $this->mobilePushNotification($devicetoken, $message, "", "", $badge, $resnot);              
              
              if($res) {
                $this->response(array('status' => true,'message' => 'Send Request Sucessfully','flag' => 1,'pushtype' =>1,'ping_create' => $created_at), 200);   
              } else {
                $this->response(array('status' => false,'message' => 'Fail to send Notification'), 200); 
              }

          } else {

              $send_at   = $created_at; 
              
              $data12 = array('sender_id'=> $sender,
                              'receiver_id' => $receiver,
                              'ping_send_status' => '1', // send the ping
                              'ping_receive_at'  => $send_at,
                              'ping_update_at'  => $send_at
                            );
              $res = $this->common_model->insert(TB_PING, $data12);

              $notdata = array('notdata_user_id'=> $receiver,
                              'notdata_device_token' =>$userReceiver[0]['user_deviceid'],
                              'notdata_type' => 'ping send',
                              'notdata_detail_id' => $res
                            );
              $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

              
              if($res) {
                $this->response(array('status' => true,'message' => 'Send Request Sucessfully','flag' => 1,'pushtype' =>1,'ping_create' => $created_at), 200);   
              } else {
                $this->response(array('status' => false,'message' => 'Fail to send Notification'), 200); 
              }


          }           

        } else {   

          $todaytime = $this->post('created_at');
          // print_r($todaytime);
          $TotalCount = $this->common_model->getUserCheck($sender,$todaytime);
          
          if(count($TotalCount) > 0) {

            $pings = $TotalCount[0]['userping_pingCount'];
            $pingCount = $pings + 1;
            $ping_id   = $TotalCount[0]['userping_id'];

            if($pings != $ping_limit) {
             
              $data2['userping_pingCount'] = $pingCount;
              $data2['userping_remainingCount'] = ($ping_limit - $pingCount);

              $this->common_model->update('tbl_userping', array('userping_id' => $ping_id,'userping_user_id' => $sender), $data2);
              $res2 = $this->common_model->Select('*','tbl_userping', array('userping_id' => $ping_id)); 

              $returnexists  = array(
                'sender'      => $sender,
                'pingcount'   => $res2[0]['userping_pingCount'],
                'remainpings' => $res2[0]['userping_remainingCount'], 
                'starttime'   => $res2[0]['created_at'], 
                'endtime'     => $res2[0]['end_time'] 
              );

              $send_at   = $created_at; 

              $data = array(
                'sender_id'=> $sender,
                'receiver_id' => $receiver,
                'ping_send_status' => '1', // send the ping
                'ping_receive_at'  => $send_at
              );

              $res = $this->common_model->insert(TB_PING, $data);

              $notdata = array('notdata_user_id'=> $receiver,
                               'notdata_device_token' =>$userReceiver[0]['user_deviceid'],
                               'notdata_type' => 'ping send',
                               'notdata_detail_id' => $res
                              );
              $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

              $badgeRows = $this->common_model->select('*', TB_NOTDATA, array('notdata_user_id' => $userReceiver[0]['user_id'], 'notdata_read_status' => 0));

              $badge = count($badgeRows); 


              $devicetoken = $userReceiver[0]['user_deviceid'];
              $message = $namesender .' '.'Pinged you, check it now!';
              $notification = $this->mobilePushNotification($devicetoken, $message, 1, $sender,$badge,$resnot);

                                   
              if($res) {

                $this->response(array('status' => true,'message' => 'Send Request Sucessfully','flag' => 1,'pushtype' =>1 , 'data' => $returnexists, 'ping_create' => $created_at), 200);   
              } else {
                $this->response(array('status' => false,'message' => 'Fail to send Notification'), 200); 
              }

            } else {
              $this->response(array('status' => true,'message' => 'Your ping limit is over', 'flag' => 3), 200); 
            } 
          } else {
              
            $data1['userping_user_id']  = $sender;
            $data1['userping_pingCount'] = '1';
            $data1['userping_remainingCount'] = ($ping_limit - 1);
            $data1['created_at'] = $this->post('created_at');
            $data1['end_time'] = date("Y-m-d H:i:s", strtotime($data1['created_at'] . $ping_hr."hours"));

            $res = $this->common_model->insert('tbl_userping', $data1);
            $id  = $this->db->insert_id(); 
            $res1 = $this->common_model->Select('*','tbl_userping', array('userping_id' => $id)); 

            $return  = array(
              'sender'      => $sender,
              'pingcount'   => $res1[0]['userping_pingCount'],
              'remainpings' => $res1[0]['userping_remainingCount'], 
              'starttime'   => $res1[0]['created_at'], 
              'endtime'     => $res1[0]['end_time'] 
            );

            $send_at   = $created_at; 

            $data = array(
              'sender_id'=> $sender,
              'receiver_id' => $receiver,
              'ping_send_status' => '1', // send the ping
              'ping_receive_at'  => $send_at
            );
            $res = $this->common_model->insert(TB_PING, $data);

            $notdata = array('notdata_user_id'=> $receiver,
                               'notdata_device_token' =>$userReceiver[0]['user_deviceid'],
                               'notdata_type' => 'ping send',
                               'notdata_detail_id' => $res
                            );
            $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

            $badgeRows = $this->common_model->select('*', TB_NOTDATA, array('notdata_user_id' => $userReceiver[0]['user_id'], 'notdata_read_status' => 0));

            $badge = count($badgeRows); 

            $devicetoken = $userReceiver[0]['user_deviceid'];
            $message = $namesender .' '.'Pinged you, check it now!';
            $notification = $this->mobilePushNotification($devicetoken, $message, 1, $sender,$badge,$resnot);

            if($res) {
              $this->response(array('status' => true,'message' => 'Send Request Sucessfully','flag' => 1,'pushtype' =>1 , 'data' => $return, 'ping_create' => $created_at), 200);   
            } else {
              $this->response(array('status' => false,'message' => 'Fail to send Notification'), 200); 
            }
          }

        } 

      } else {

          $send_at   = $created_at; 
          $time = date('Y-m-d H:i:s', time() + 30);


          $data = array(
            'sender_id'=> $sender,
            'receiver_id' => $receiver,
            'ping_send_status' => '1', // send the ping
            'ping_receive_at'  => $send_at,
            'ping_unping_at'  => $time,
            'ping_notify_send'  => 1
          );
          // print_r($data); die();
          $res = $this->common_model->insert(TB_PING, $data);

          $notdata = array('notdata_user_id'=> $receiver,
                               'notdata_device_token' =>$userReceiver[0]['user_deviceid'],
                               'notdata_type' => 'ping send',
                               'notdata_detail_id' => $res
                            );
          $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

          if($res) {
            $this->response(array('status' => true,'message' => 'Send Request Sucessfully','flag' => 1,'pushtype' =>1, 'ping_create' => $created_at), 200);   
          } else {
            $this->response(array('status' => false,'message' => 'Fail to send Notification'), 200); 
          }          
        } 

      } else {

        $existsSender = $checkExists[0]['sender_id'];
        $existsAcceptStatus = $checkExists[0]['ping_accept_status'];
              if(count($checkExists) > 0) {
                  $ping_create = $checkExists[0]['ping_receive_at'];   
              }

        if($existsSender == $sender) {

          if($existsAcceptStatus == 0) {

            $this->response(array('status' => true,'message' => 'You already send request', 'flag' => 4, 'ping_create' => $ping_create), 200);
          } else {
            $this->response(array('status' => true,'message' => 'Request already accepted', 'flag' => 5), 200);
          } 

        } else {

          if($existsAcceptStatus == 0) {

            $this->response(array('status' => true,'message' => 'You already receive request', 'flag' => 6), 200);
          } else {
            $this->response(array('status' => true,'message' => 'You already accept request', 'flag' => 5), 200);
          }        
        }

      }  

    }

        if($flag == '2') {   // 2 status for accept request

            $res = $this->common_model->Select('*',TB_PING, array('sender_id' => $receiver, 'receiver_id' => $sender)); 
            //echo count($res); die();
            if(count($res) > 0) {

              if($res[0]['ping_accept_status'] == 0) {

                if($userReceiver[0]['user_loginstatus'] == 1) {
       
                  $notdata = array('notdata_user_id'=> $receiver,
                                   'notdata_device_token' =>$userReceiver[0]['user_deviceid'],
                                   'notdata_type' => 'ping accept',
                                   'notdata_detail_id' => $res[0]['ping_id']
                                );
                  $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

                  if($resnot) {      
                    $data['ping_accept_status'] = '2';  // accept ping request
                    $data['ping_accept_at']     = $this->post('created_at'); 
                    $data['ping_update_at']     = $this->post('created_at'); 

                    $this->common_model->update(TB_PING, array('sender_id' => $receiver,'receiver_id' => $sender), $data);

                    $badgeRows = $this->common_model->select('*', TB_NOTDATA, array('notdata_user_id' => $userReceiver[0]['user_id'], 'notdata_read_status' => 0));

                    $badge = count($badgeRows); 

                    $devicetoken  = $userReceiver[0]['user_deviceid'];
                    $message      =  'Congrats! You have a new Bond '. $namesender;
                    $notification = $this->mobilePushNotification($devicetoken, $message, 2, $sender, $badge, $resnot);
                    // $notification = $this->mobilePushNotificationChat($devicetoken, $message, 2, $sender,$namesender, $badge, $resnot);


                    $this->response(array('status' => true,'message' => 'Accept request','flag' => 2,'pushtype' =>1 , 'data' => $receiver), 200); 
                  } else {
                    $this->response(array('status' => false,'message' => 'Fail to send Notification'), 200); 
                  }  

                } else {

                      $notdata = array('notdata_user_id'=> $receiver,
                                       'notdata_device_token' =>$userReceiver[0]['user_deviceid'],
                                       'notdata_type' => 'ping accept',
                                       'notdata_detail_id' => $res[0]['ping_id']
                                    );
                      $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

                    if($resnot) {      
                        $data['ping_accept_status'] = '2';  // accept ping request
                        $data['ping_accept_at']     = $this->post('created_at'); 
                        $data['ping_update_at']     = $this->post('created_at'); 

                        $this->common_model->update(TB_PING, array('sender_id' => $receiver,'receiver_id' => $sender), $data);

                        $this->response(array('status' => true,'message' => 'Accept request','flag' => 2,'pushtype' =>1 , 'data' => $receiver), 200); 
                    } else {
                        $this->response(array('status' => false,'message' => 'Fail to send Notification'), 200); 
                    }  

                }  

              } else {

                    $this->response(array('status' => false,'message' => 'Already accept request', 'flag' => 5), 200); 
              }   

            } else {
              $this->response(array('status' => false,'message' => 'Record is not present'), 200); 
            }        
        }


        if($flag == '3') {   // 3 status for reject request

            $res = $this->common_model->Select('*',TB_PING, array('sender_id' => $receiver, 'receiver_id' => $sender)); 
            if(count($res) > 0) {

              $data['ping_status'] = '3';  // reject ping request
              // $result = $this->common_model->update(TB_PING, array('sender_id' => $receiver,'receiver_id' => $sender), $data);
              $result = $this->common_model->delete(TB_PING, array('sender_id' => $receiver,'receiver_id' => $sender));
             
              if($result) {
                $this->response(array('status' => true,'message' => 'Reject Request','flag' => 3,'pushtype' =>1 , 'data' => $sender), 200); 
              } else {
                $this->response(array('status' => false,'message' => 'Fail to request request'), 200); 
              }
            } else {
              $this->response(array('status' => false,'message' => 'Record is not present'), 200); 
            }
        } 


        if($flag == '4') {   // 4 status for cancelled request
        
            $res = $this->common_model->Select('*',TB_PING, array('sender_id' => $sender, 'receiver_id' => $receiver)); 
            //print_r($res); die();
              if(count($res) > 0) {

                $data['ping_status'] = '4';  // cancelled ping request
                $result = $this->common_model->delete(TB_PING, array('sender_id' => $sender,'receiver_id' => $receiver));

                if($result) {
                  $this->response(array('status' => true,'message' => 'Cancelled ping','flag' => 4,'pushtype' =>1 , 'data' => $sender), 200); 
                } else {
                  $this->response(array('status' => false,'message' => 'Fail to cancel ping'), 200); 
                }
            } else {
              $this->response(array('status' => false,'message' => 'Record is not present'), 200); 
            }
        }        

    } else {
      $this->response(array('status' => false,'message' => 'Sender Or Receiver User Not Exists'), 200); 
    }

  } else {
  $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
  }      
  
} 


// LIST OD NOTIFICATIONS, THE USER GET

  public function notificationList_post() {

    if (trim($this->post('user_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
    }
    
    $userid = $this->post('user_id'); 

 $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
  if(count($userExists) > 0) {    
    
    $notifreceiveUnread = $this->common_model->notifreceiveUnread($userid);


      $receiveUnread = array();
      if(count($notifreceiveUnread) > 0) {

        for($k=0; $k < count($notifreceiveUnread); $k++) {
          $receiverUnreadData = $this->common_model->select('*', TB_USER, array('user_id' => $notifreceiveUnread[$k]['sender_id'])); 
          if($receiverUnreadData[0]['user_profileimage'] != ''){
            $receiveUnread[$k]['user_profileimage'] = base_url().'uploads/profileimages/'.$receiverUnreadData[0]['user_profileimage'];
          } 

          if($receiverUnreadData[0]['user_lastname'] != '') {
            $receiveUnread[$k]['username'] = $receiverUnreadData[0]['user_firstname'].$receiverUnreadData[0]['user_lastname'];
          } else {
            $receiveUnread[$k]['username'] = $receiverUnreadData[0]['user_firstname'];
          }

          $receiveUnread[$k]['user_id'] = $receiverUnreadData[0]['user_id'];
          $receiveUnread[$k]['date'] = $notifreceiveUnread[$k]['ping_receive_at'];
          //$receiveUnread[$k]['read_status'] = $notifreceiveUnread[$k]['ping_read_status'];
          if($notifreceiveUnread[$k]['receiver_id'] == $this->post('user_id') &&  $notifreceiveUnread[$k]['ping_accept_status'] == 0 && $notifreceiveUnread[$k]['ping_notify_send'] == 0 ){
            $receiveUnread[$k]['message'] = "You received request from " . $receiveUnread[$k]['username'];  
            $receiveUnread[$k]['user_onlinestatus'] = '';
            $receiveUnread[$k]['notflag'] = '1';      // RECEIVE REQUEST
          }else{
            $receiveUnread[$k]['message'] = "You accepted request from " . $receiveUnread[$k]['username']; 
            $receiveUnread[$k]['user_onlinestatus'] = '';
            $receiveUnread[$k]['notflag'] = '2';      // FOR ACCEPT REQUEST
          }
          
          if($notifreceiveUnread[$k]['ping_read_status'] == '0') {
            $receiveUnread[$k]['id'] = $notifreceiveUnread[$k]['ping_id'];            
          } 
        }
      }


//OTHER USER ACCEPT YOUR REQUEST
      $notifaccept = $this->common_model->notifaccept($userid);
      

      if(count($notifaccept) > 0) {

        for($j=0; $j < count($notifaccept); $j++) {

          $acceptData = $this->common_model->select('*', TB_USER, array('user_id' => $notifaccept[$j]['receiver_id'])); 

          if($acceptData[0]['user_profileimage'] != ''){
            $accept[$j]['user_profileimage'] = base_url().'uploads/profileimages/'.$acceptData[0]['user_profileimage'];
          } 

          if($acceptData[0]['user_lastname'] != '') {
            $accept[$j]['username'] = $acceptData[0]['user_firstname'].$acceptData[0]['user_lastname'];
          } else {
            $accept[$j]['username'] = $acceptData[0]['user_firstname'];
          }

          $accept[$j]['user_id'] = $acceptData[0]['user_id'];
          $accept[$j]['date'] = $notifaccept[$j]['ping_accept_at'];
         // $accept[$j]['read_status'] = $notifaccept[$j]['ping_read_status'];
          $accept[$j]['message'] = $accept[0]['username']. " Accepted your request";
          $accept[$j]['user_onlinestatus'] = '';
          $accept[$j]['notflag'] = '2';        // FOR ACCEPT REQUEST
        }     
      }  
      

// CHAT LIST 

    $chats = $this->common_model->selectChatList($userid);


    if(count($chats) > 0) {

      for($i=0; $i < count($chats); $i++) {

        if($chats[$i]['sender_id'] == $userid) {

          $Data1 = $this->common_model->select('*', TB_USER, array('user_id' => $chats[$i]['receiver_id']));
        } else {
          $Data1 = $this->common_model->select('*', TB_USER, array('user_id' => $chats[$i]['sender_id']));
        }

        if($Data1[0]['user_profileimage'] != ''){
          $chatdata1[$i]['user_profileimage'] = base_url().'uploads/profileimages/'.$Data1[0]['user_profileimage'];
        } 

        if($Data1[0]['user_lastname'] != '') {
          $chatdata1[$i]['username'] = $Data1[0]['user_firstname'].$Data1[0]['user_lastname'];
        } else {
          $chatdata1[$i]['username'] = $Data1[0]['user_firstname'];
        }

        $chatdata1[$i]['user_id'] = $Data1[0]['user_id'];
        $chatdata1[$i]['date'] = $chats[$i]['ping_update_at'];
        $chatdata1[$i]['message'] = $Data1[0]['user_description'];
        $chatdata1[$i]['user_onlinestatus'] = $Data1[0]['user_onlinestatus'];
        $chatdata1[$i]['notflag']           = '3';      // FOR CHAT
      }
    }     


// MEMBERSHIP PURCHASE NOTIFICATION LIST 

    $purchase1 = $this->common_model->select('*', TB_NOT, array('user_id' => $userid));
    $name = $this->common_model->select('*', TB_USER, array('user_id' => $userid));

        if($name[0]['user_lastname'] != '') {
          $pur['username'] = $name[0]['user_firstname'].$name[0]['user_lastname'];
        } else {
          $pur['username'] = $name[0]['user_firstname'];
        }

    if(count($purchase1) > 0) {

      for($l=0; $l < count($purchase1); $l++) {

        $purchase[$l]['user_profileimage'] = base_url().'uploads/profileimages/'.$name[0]['user_profileimage'];;
        $purchase[$l]['username'] = $pur['username'];
        $purchase[$l]['user_id'] = $purchase1[$l]['user_id'];
        $purchase[$l]['date']    = $purchase1[$l]['created_at'];
        $purchase[$l]['message'] = $purchase1[$l]['notification_msg'];
        $purchase[$l]['user_onlinestatus'] = '';
        $purchase[$l]['notflag']           = '4';     
      }
    }     

      if(isset($receiveUnread) && isset($accept) && isset($chatdata1) && isset($purchase)) {
       $data = array_merge($receiveUnread,$accept,$chatdata1,$purchase); 
      }
      else if(isset($receiveUnread) && isset($accept) && isset($chatdata1)) {
       $data = array_merge($receiveUnread,$accept,$chatdata1); 
      }
      else if(isset($receiveUnread) && isset($chatdata1) && isset($purchase)) {
       $data = array_merge($receiveUnread,$chatdata1,$purchase); 
      }
      else if(isset($receiveUnread) && isset($accept) && isset($purchase)) {
       $data = array_merge($receiveUnread,$accept,$purchase); 
      }
      else if(isset($accept) && isset($chatdata1) && isset($purchase)) {
       $data = array_merge($accept,$chatdata1,$purchase); 
      } 
      else if(isset($receiveUnread) && isset($accept)) {
       $data = array_merge($receiveUnread,$accept); 
      } 
      else if(isset($accept) && isset($chatdata1)) {
       $data = array_merge($accept,$chatdata1); 
      } 
      if(isset($receiveUnread) && isset($chatdata1)) {
       $data = array_merge($receiveUnread,$chatdata1); 
      } 
      else if(isset($receiveUnread) && isset($purchase)) {
       $data = array_merge($receiveUnread,$purchase); 
      } 
      else if(isset($accept) && isset($purchase)) {
       $data = array_merge($accept,$purchase); 
      } 
      else if(isset($chatdata1) && isset($purchase)) {
       $data = array_merge($chatdata1,$purchase);          
      } 
      else if(isset($accept)) {
        $data = $accept;  
      } 
      else if(isset($receiveUnread)) {
        $data = $receiveUnread;  
      }
      else if(isset($chatdata1)) {
        $data = $chatdata1;  
      } 
      else if(isset($purchase)) {
        $data = $purchase;  
      }
      
      $date = array();
      foreach ($data as $key => $row)
      {
          $date[$key] = $row['date'];
      }
      array_multisort($date, SORT_DESC, $data);

      if(isset($data) && $data != 'null') {
        $this->response(array('status' => true, 'message' => 'All Notification and chat Data', 'data' =>$data), 200);   
      } else {
        $this->response(array('status' => false, 'message' => 'No data found'), 200);   
      } 

    } else {
    $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    }        

  }       


// CHANGE READ STATUS FOR NOTIFICATION

  public function statusNotification_post() {

    if (trim($this->post('id')) == "") {
      $this->response(array('status' => false, 'message' => 'Enter id'), 200);
    }
    $result = $this->common_model->update(TB_PING, array('ping_id' => $this->post('id')), array('ping_read_status' => 1));
    if($result) {
      $this->response(array('status' => true, 'message' => 'Read Notification'), 200);     
    } else {
      $this->response(array('status' => false, 'message' => 'Fail To Read'), 200);     
    }
  }

// LIST OF MEMBERSHIP 

  public function listMembership_post() {

  $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
    if(count($userExists) > 0) {        

        if (trim($this->post('user_id')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
        }

        $exists = $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id'))); 
          $userMembership = $exists[0]['user_membership_id'];
          $membership = $this->common_model->listMembership();

        if($membership) {
          $this->response(array('status' => true, 'message' => 'list of memebership data', 'data' => $membership,'membership' => $userMembership), 200);     
        } else {
          $this->response(array('status' => false, 'message' => 'No Data Found'), 200);     
        }

    } else {
    $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    } 

  }


// In App purchase Api

  public function inappPurchase_post() {

    $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
    if(count($userExists) > 0) {      

        if (trim($this->post('user_id')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
        } 
        if (trim($this->post('membership_id')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter membership id'), 200);
        } 
        if (trim($this->post('transaction_id')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter transaction id'), 200);
        }
        $user_id = $this->post('user_id'); 

        $memberData = $this->common_model->select('*', TB_MEMBERSHIPS, array('membership_id' => $this->post('membership_id')));
        $member_period = $memberData[0]['membership_period'];
        $member_time   = $memberData[0]['membership_time'];
        $member_type   = $memberData[0]['membership_type'];
        $member_tokens = $memberData[0]['membership_tokens'];

      if($member_type == 'Monthly') {

          $starttime   = $this->post('starttime');

          if($member_period == 'min') {
            $endtime = date('Y-m-d H:i:s',strtotime($member_time.'minutes', strtotime($starttime)));
          }
          if($member_period == 'hr') {
            $endtime = date('Y-m-d H:i:s',strtotime($member_time.'hours', strtotime($starttime)));
          } 
          if($member_period == 'day') {
            $endtime = date('Y-m-d H:i:s',strtotime($member_time.'day', strtotime($starttime)));
          }

            $data = array('user_id'        => $this->post('user_id'),
                          'membership_id'  => $this->post('membership_id'),
                          'transaction_id' => $this->post('transaction_id'),
                          'status'         => 1,
                          'start_time'     => $starttime,
                          'end_time'       => $endtime
              );
            $result = $this->common_model->insert(TB_PURCHASE, $data);

            $sub    = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_subscription' => 1));
            $userMem = $this->common_model->select('*', TB_USER, array('user_id' => $user_id));


            $notdata = array('notdata_user_id'=> $userMem[0]['user_id'],
                             'notdata_device_token' =>$userMem[0]['user_deviceid'],
                             'notdata_type' => 'membership'
                            );
            $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

            $badgeRows = $this->common_model->select('*', TB_NOTDATA, array('notdata_user_id' => $userMem[0]['user_id'], 'notdata_read_status' => 0));

            $badge = count($badgeRows);

            $devicetoken  = $userMem[0]['user_deviceid'];
            $message      =  'You purchase the membership plan successfully';
            $notification = $this->mobilePushNotification($devicetoken, $message, 4, $user_id, $badge, $resnot);
            $noti['user_id'] = $user_id;
            $noti['notification_msg'] = $message;
            $not = $this->common_model->insert(TB_NOT, $noti);

              if($result && $not) {
                 $this->response(array('status' => true, 'message' => 'You get membership plan', 'data' =>$data), 200); 
              } else {
                 $this->response(array('status' => false, 'message' => 'Fail to membership plan'), 200); 
              }

      } else {

            $data1 = array('user_id'   => $this->post('user_id'),
                           'membership_id'  => $this->post('membership_id'),
                           'transaction_id' => $this->post('transaction_id'),
                           'status'         => 0
                          );
            $result = $this->common_model->insert(TB_PURCHASE, $data1);

            $userExists = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $user_id));

              if(count($userExists) > 0) {

                  $total = $userExists[0]['tokens_total'] + $member_tokens;
                  $purchase = $userExists[0]['tokens_purchase'] + $member_tokens;
                  $remain = $userExists[0]['tokens_remain'] + $member_tokens;
                   
                  $dataUpdate = array('tokens_total'    => $total,
                                       'tokens_purchase' => $purchase,
                                       'tokens_remain'   => $remain
                                      );
                  $result = $this->common_model->update(TB_TOKEN, array('tokens_user_id' => $user_id), $dataUpdate);
                 
                  $remainTokens = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $user_id));


                  $data  = array('tokens_remain' => $remainTokens[0]['tokens_remain']
                                );

                  // $usertokens = $this->common_model->select('*', TB_PURCHASE, array('purchase_id' => $result));

                  // $data  = array('purchase_id'   => $usertokens[0]['purchase_id'],
                  //                'user_id'       => $usertokens[0]['user_id'],
                  //                'status'        => $usertokens[0]['status']
                  //               );

              } else {

                  $data1 = array('tokens_user_id'  => $user_id,
                           'tokens_total'    => $member_tokens,
                           'tokens_purchase' => $member_tokens,
                           'tokens_remain'   => $member_tokens
                          );
                  $result = $this->common_model->insert(TB_TOKEN, $data1);

                  $remainTokens = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $user_id));

                  $data  = array('tokens_remain' => $remainTokens[0]['tokens_remain']
                                );

                  // $usertokens = $this->common_model->select('*', TB_PURCHASE, array('purchase_id' => $result));

                  // $data  = array('purchase_id'   => $usertokens[0]['purchase_id'],
                  //                'user_id'       => $usertokens[0]['user_id'],
                  //                'status'        => $usertokens[0]['status']
                  //               );
              }
            

            


            $userMem = $this->common_model->select('*', TB_USER, array('user_id' => $user_id));


            $notdata = array('notdata_user_id'=> $userMem[0]['user_id'],
                             'notdata_device_token' =>$userMem[0]['user_deviceid'],
                             'notdata_type' => 'membership'
                            );
            $resnot = $this->common_model->insert(TB_NOTDATA, $notdata);

            $badgeRows = $this->common_model->select('*', TB_NOTDATA, array('notdata_user_id' => $userMem[0]['user_id'], 'notdata_read_status' => 0));

            $badge = count($badgeRows);

            $devicetoken  = $userMem[0]['user_deviceid'];
            $message      =  'You purchase the membership plan successfully';
            $notification = $this->mobilePushNotification($devicetoken, $message, 4, $user_id, $badge, $resnot);
            $noti['user_id'] = $user_id;
            $noti['notification_msg'] = $message;
            $not = $this->common_model->insert(TB_NOT, $noti);          

              if($result && $not) {
                 $this->response(array('status' => true, 'message' => 'You get membership plan', 'data' =>$data), 200);  
              } else {
                 $this->response(array('status' => false, 'message' => 'Fail to membership plan'), 200); 
              }
      }        


    } else {
    $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    } 

  } 


  public function tokenStart_post() {

    if (trim($this->post('user_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
    }
    if (trim($this->post('starttime')) == "") {
      $this->response(array('status' => false, 'message' => 'Enter start time'), 200);
    }    

      $user_id     = $this->post('user_id');
      $starttime   = $this->post('starttime');
    $userMem = $this->common_model->select('*', TB_USER, array('user_id' => $user_id));

      if(count($userMem) > 0) { 

        $status = $userMem[0]['user_status']; 

        if($status == 0) {
           
            $tokens = $this->common_model->select('*', TB_TOKEN, array('tokens_user_id' => $user_id));

            if(count($tokens) > 0) {

              if($tokens[0]['tokens_remain'] > '0') {
            
                  $data1['tokens_remain'] = $tokens[0]['tokens_remain'] - 1;
                  $data1['tokens_start_time']             = $this->post('starttime');
                  $data1['tokens_end_time']               = date('Y-m-d H:i:s',strtotime('30 minutes', strtotime($starttime)));

                  $membership = $this->common_model->update(TB_TOKEN, array('tokens_user_id' => $user_id), $data1);

                  $sub = $this->common_model->update(TB_USER, array('user_id' => $user_id), array('user_subscription' => 2));

                    if($membership) {

                      $this->response(array('status' => true, 'message' => 'Token is activated', 'data' => $data1), 200);
                    } else {
                      $this->response(array('status' => false, 'message' => 'Fail to activate'), 200);
                    }

               } else {
                $this->response(array('status' => false, 'message' => 'No tokens remain'), 200);
               } 

            } else {
               $this->response(array('status' => false, 'message' => 'Please purchase token'), 200);
            }  
            
        } else {

           $this->response(array('status' => false, 'message' => 'Account deactivated'), 200);
        }    

      } else {

          $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
      }  

  }   


  public  function mobilePushNotification($devicetoken, $message, $id, $sender,$badge,$resnot) {
    $passphrase = '1234etpl';
      
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushcert.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

    $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    $body['aps'] = array(
      'alert' => array(
        'body' => $message,
        'action-loc-key' => 'BondApp',        
      ),
      'status' => $id,
      'sender_id' => $sender,
      'badge' => $badge,
      'notification_read' => $resnot,
      'sound' => 'oven.caf',
    );
    // Encode the payload as JSON
    $payload = json_encode($body);
    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $devicetoken) . pack('n', strlen($payload)) . $payload;
    $result = fwrite($fp, $msg, strlen($msg));
    //socket_close($fp);
    fclose($fp);
    return $result;
}  


// READ AND UNREAD STATUS FOR NOTIFICATION BADGE

  public function badgeRead_post() {
    if (trim($this->post('notification_read_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Enter notification id'), 200);
    }    

    $sub = $this->common_model->update(TB_NOTDATA, array('notdata_id' => $this->post('notification_read_id')), array('notdata_read_status' => 1));
      if($sub) {
        $this->response(array('status' => true, 'message' => 'Notification is read'), 200); 
      } else {
        $this->response(array('status' => false, 'message' => 'Fail to read notification'), 200); 
      }

  } 


// CHAT LIST WHO ACCEPT THE REQUEST

  public function chatlist_post() {

$userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
    if(count($userExists) > 0) {     

    if (trim($this->post('user_id')) == "") {
      $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
    }
    $user_id  = $this->post('user_id');

    $chatsSend = $this->common_model->select('*', TB_PING, array('sender_id' => $user_id, 'ping_accept_status' => 2));
    $chatsReceive = $this->common_model->select('*', TB_PING, array('receiver_id' => $user_id, 'ping_accept_status' => 2));

    if(count($chatsSend) > 0) {

      for($i=0; $i < count($chatsSend); $i++) {

        $Data = $this->common_model->select('*', TB_USER, array('user_id' => $chatsSend[$i]['receiver_id'])); 

        if($Data[0]['user_profileimage'] != ''){
          $chatdata[$i]['user_profileimage'] = base_url().'uploads/profileimages/'.$Data[0]['user_profileimage'];
        } 

        if($Data[0]['user_lastname'] != '') {
          $chatdata[$i]['username'] = $Data[0]['user_firstname'].$Data[0]['user_lastname'];
        } else {
          $chatdata[$i]['username'] = $Data[0]['user_firstname'];
        }

        $chatdata[$i]['user_id'] = $Data[0]['user_id'];
        $chatdata[$i]['user_description'] = $Data[0]['user_description'];
        $chatdata[$i]['user_onlinestatus'] = $Data[0]['user_onlinestatus'];
        $chatdata[$i]['notflag']           = '3';
      }
    }  

    if(count($chatsReceive) > 0) {

      for($i=0; $i < count($chatsReceive); $i++) {

        $Data1 = $this->common_model->select('*', TB_USER, array('user_id' => $chatsReceive[$i]['sender_id'])); 

        if($Data1[0]['user_profileimage'] != ''){
          $chatdata1[$i]['user_profileimage'] = base_url().'uploads/profileimages/'.$Data1[0]['user_profileimage'];
        } 

        if($Data1[0]['user_lastname'] != '') {
          $chatdata1[$i]['username'] = $Data1[0]['user_firstname'].$Data1[0]['user_lastname'];
        } else {
          $chatdata1[$i]['username'] = $Data1[0]['user_firstname'];
        }

        $chatdata1[$i]['user_id'] = $Data1[0]['user_id'];
        $chatdata1[$i]['user_description'] = $Data1[0]['user_description'];
        $chatdata1[$i]['user_onlinestatus'] = $Data1[0]['user_onlinestatus'];
        $chatdata1[$i]['notflag']           = '3';
      }
    }  
    if(isset($chatdata) && isset($chatdata1)) {
      $data = array_merge($chatdata, $chatdata1);
    } else if(isset($chatdata)) {
      $data = $chatdata;
    } else if(isset($chatdata1)) {
      $data = $chatdata1;
    }

    if(isset($data)) {
      $this->response(array('status' => true, 'message' => 'Chat list data', 'data' =>$data), 200);     
    } else {
      $this->response(array('status' => true, 'message' => 'No Data Found'), 200);     
    } 

  } else {
  $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
  } 

  }


// change subscription status to Free user

  public function subscriptionFree_post() {

   $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
    if(count($userExists) > 0) { 

        if (trim($this->post('user_id')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
        }

        $sub = $this->common_model->update(TB_USER, array('user_id' => $this->post('user_id')), array('user_subscription' => 0, 'user_membership_id' => 0));
          if($sub) {
            $this->response(array('status' => true, 'message' => 'Your membership plan is over'), 200); 
          } else {
            $this->response(array('status' => false, 'message' => 'Fail to change status'), 200); 
          }

  } else {
  $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
  }      

  }

// Delete chat

  public function deleteChatMsg_post() {

  $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
    if(count($userExists) > 0) { 

        if (trim($this->post('user_id')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
        }
        if (trim($this->post('chat_id')) == "") {
          $this->response(array('status' => false, 'message' => 'Enter chat id'), 200);
        }
        $user_id = $this->post('user_id');
        $chat_id = $this->post('chat_id');
          $chats = explode(",",$chat_id);
           for($i=0; $i < count($chats); $i++) {
              $result = $this->db->query('DELETE `tbl_chat` WHERE `chat_id`  = "'.$chats[$i].'" ');
           }
              if($result) {
                $this->response(array('status' => true,'message' => 'delete chat'), 200);
              } else {
                $this->response(array('status' => false,'message' => 'Fail to delete'), 200);
              }

  } else {
  $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
  }  

  } 

// Received Ping Request

  public function pingRequestUsers_post() {
    if (trim($this->post('user_id')) == "") {
        $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
    }
        $userId = $this->post('user_id');
        $userExists = $this->common_model->select('*', TB_USER, array('user_id' => $userId));
        
        if(count($userExists) > 0) {

          $pingReceive= $this->common_model->select('*', TB_PING, array('receiver_id' => $userId, 'ping_accept_status' => '0'));


          if(count($pingReceive) > 0) {

            for($i=0; $i < count($pingReceive); $i++) {

              $Data = $this->common_model->select('*', TB_USER, array('user_id' => $pingReceive[$i]['sender_id'])); 
              // print_r($Data); die();
              if(count($Data) > 0) {

                  if($Data[0]['user_profileimage'] != ''){
                    $receivePing[$i]['user_profileimage'] = base_url().'uploads/profileimages/'.$Data[0]['user_profileimage'];
                  } 

                    $receivePing[$i]['user_firstname']    = $Data[0]['user_firstname'];
                    $receivePing[$i]['user_lastname']     = $Data[0]['user_lastname'];
                    $receivePing[$i]['user_id']           = $Data[0]['user_id'];
                    $receivePing[$i]['user_onlinestatus'] = $Data[0]['user_onlinestatus'];
                    
              } else {
                $this->response(array('status' => false, 'message' => 'No data found'), 200);
              }      
             
            } 
              $this->response(array('status' => true, 'message' => 'Ping received', 'data' => $receivePing), 200);

          } else {
              $this->response(array('status' => false, 'message' => 'No ping received'), 200);
          }             

        } else {

              $this->response(array('status' => false, 'message' => 'User not exists'), 200);
        }
  }

// Chat Users

  public function chatRequestUsers_post() {
    if (trim($this->post('user_id')) == "") {
        $this->response(array('status' => false, 'message' => 'Enter user id'), 200);
    }
        $userId = $this->post('user_id');
        $userExists = $this->common_model->select('*', TB_USER, array('user_id' => $userId));
        
        if(count($userExists) > 0) {

          $pingReceive = $this->common_model->select('*', TB_PING, array('receiver_id' => $userId, 'ping_accept_status' => '2'));  

          if(count($pingReceive) > 0) {

              for($i=0; $i < count($pingReceive); $i++) {
    
                  $Data = $this->common_model->select('*', TB_USER, array('user_id' => $pingReceive[$i]['sender_id'])); 

                  if(count($Data) > 0) {

                      if($Data[0]['user_profileimage'] != ''){
                        $receive[$i]['user_profileimage'] = base_url().'uploads/profileimages/'.$Data[0]['user_profileimage'];
                      } 

                        $receive[$i]['user_firstname']    = $Data[0]['user_firstname'];
                        $receive[$i]['user_lastname']     = $Data[0]['user_lastname'];
                        $receive[$i]['user_id']           = $Data[0]['user_id'];
                        $receive[$i]['user_onlinestatus'] = $Data[0]['user_onlinestatus'];
                  } 
              } 
          }  

          $pingSender = $this->common_model->select('*', TB_PING, array('sender_id' => $userId, 'ping_accept_status' => '2'));  

          if(count($pingSender) > 0) {

              for($j=0; $j < count($pingSender); $j++) {
    
                  $Data1 = $this->common_model->select('*', TB_USER, array('user_id' => $pingSender[$j]['receiver_id'])); 

                  if(count($Data1) > 0) {

                      if($Data1[0]['user_profileimage'] != ''){
                        $sender[$j]['user_profileimage'] = base_url().'uploads/profileimages/'.$Data1[0]['user_profileimage'];
                      } 

                        $sender[$j]['user_firstname']    = $Data1[0]['user_firstname'];
                        $sender[$j]['user_lastname']     = $Data1[0]['user_lastname'];
                        $sender[$j]['user_id']           = $Data1[0]['user_id'];
                        $sender[$j]['user_onlinestatus'] = $Data1[0]['user_onlinestatus'];
                  } 
              } 
          }  

          if(isset($receive) && isset($sender)) {
            $receivePing = array_merge($receive, $sender);
          } else if(isset($receive)) {
            $receivePing = $receive;
          } else if(isset($sender)) {
            $receivePing = $sender;
          }

            $this->response(array('status' => true, 'message' => 'chat user', 'data' => $receivePing), 200);

        } else {

            $this->response(array('status' => false, 'message' => 'User not exists'), 200);
        }
  }

 
  public function logout_post() {

    $userExists= $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id')));
      if(count($userExists) > 0) {    
          
          $userArr = $this->common_model->select('*', TB_USER, array('user_id' => $this->post('user_id'), 'user_verified' => 1));

          if (count($userArr) > 0) {
            $this->common_model->update(TB_USER, array('user_id' => $this->post('user_id')), array('user_loginstatus' => 0));
            $this->response(array('status' => true, 'message' => 'Logout sucessfully'), 200);
          } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong. !!'), 200);
          }

    } else {
    $this->response(array('status' => false, 'message' => 'User not exists', 'userActiveFlag' => 0), 200);
    } 
        
  }



}
