<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'BondApp : Dashboard';
        $data['userArr']  = $this->user_model->selectrow_count('user_id', TB_USER,array());
       // $userArr = $this->user_model->select('user_id', 'tbl_users', array('user_age' => BETWEEN '18' AND '25'));
        $RangeFirst       = $this->user_model->getUsersAgeRangeFirst();
        $RangeSecond      = $this->user_model->getUsersAgeRangeSecond();
        $RangeThird       = $this->user_model->getUsersAgeRangeThird();
        $RangeFourth      = $this->user_model->getUsersAgeRangeFourth();


        $data['RangeFirst']     = ($RangeFirst / $data['userArr']) * 100;
        $data['RangeSecond']    = ($RangeSecond / $data['userArr']) * 100;
        $data['RangeThird']     = ($RangeThird / $data['userArr']) * 100;
        $data['RangeFourth']    = ($RangeFourth / $data['userArr']) * 100;
  
         $data['DistinctCountry'] = $this->user_model->getDistinctCountry(); 
         $users = $this->user_model->getGraphCount();
         $datas = '';

        for ($i=0; $i < count($users); $i++) { 
          $datas .= '[gd('.$users[$i]['year'].', 1, '.$users[$i]['month'].'), '.$users[$i]['total'].'],';      
        }         
         $data['graph']         = rtrim($datas) ; 

        $this->loadViews("admin/dashboard", $this->global, $data , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    function userManagement()
    {
        $this->global['pageTitle'] = 'BondApp : User Management';
        $data['AllUsersdata'] = $this->user_model->getAllUsers();
       
        $this->loadViews("admin/user_management", $this->global, $data , NULL);
    }

    // Delete Users in user management

    function deleteUserManagement($user_id) {
         $result = $this->user_model->delete(TB_USER, array('user_id' => $user_id));
           if($result) {
              $this->session->set_flashdata('success', 'User Deleted Successfully'); 
              redirect('userManagement');       
           }
        
    }

    function deactivateUserManagement($user_id) {

        $data = array('user_status' => '1');
         $result = $this->user_model->update(TB_USER, array('user_id' => $user_id), $data);
           if($result) {
              $this->session->set_flashdata('success', 'User Deactivated Successfully'); 
              redirect('userManagement');       
           }
        
    } 

    function activateUserManagement($user_id) {

        $data = array('user_status' => '0');
         $result = $this->user_model->update(TB_USER, array('user_id' => $user_id), $data);
           if($result) {
              $this->session->set_flashdata('success', 'User Activated Successfully'); 
              redirect('userManagement');       
           }
        
    } 


    function allRequest()
    {
        $this->global['pageTitle'] = 'BondApp : Request';
        $data['AllUsersdata'] = $this->user_model->selectReplyRequest();
        $this->loadViews("admin/all_request", $this->global, $data , NULL);
    }
    

   function deactivateUserOnRequest($user_id) {

        $data = array('user_status' => '1');
         $result = $this->user_model->update(TB_USER, array('user_id' => $user_id), $data);
           if($result) {
              $this->session->set_flashdata('success', 'User Deactivated Successfully'); 
              redirect('allRequest');       
           }
        
    }   



    function financialManagement()
    {
        $this->global['pageTitle'] = 'BondApp : Financial';
        $data['UsersPurchasedata'] = $this->user_model->getpurchse_users();
       $data['PurchaseIssue']     = $this->user_model->select('*', TB_REQUEST, array('request_type' => 'Payment Issue','reply_status' =>'0'));
        $this->loadViews("admin/financial_management", $this->global, $data , NULL);
    }

    function settings()
    {
        $this->global['pageTitle'] = 'BondApp : settings';
         $data['range'] = $this->user_model->select('*', TB_RANGE, array('range_id' => 1));
         $data['membership'] = $this->user_model->getSettings();
         $data['ping'] = $this->user_model->select('*', TB_FREEPING, array());
        $this->loadViews("admin/settings", $this->global,$data , NULL);
    }
   

    function promotions()
    {
        $this->global['pageTitle'] = 'BondApp : promotions';
        //$data['UsersPurchasedata'] = $this->user_model->getpurchse_users();
        $this->loadViews("admin/promotions", $this->global,NULL , NULL);
    }

    function statistics()
    {
        $this->global['pageTitle'] = 'BondApp : statistics';
        $data['purchaseCount']   = $this->user_model->getPurchaseUsersCount();
        $data['Monthly'] = $this->user_model->getMembershipMonthly(); 
        $data['Token'] = $this->user_model->getMembershipToken();  
        $data['genderCountToken']   = $this->user_model->getDistinctGenderToken();
        $data['genderCountMonthly']   = $this->user_model->getDistinctGenderMonthly();
        $this->loadViews("admin/statistics", $this->global,$data , NULL);
    }  


  /**
     * This function is used to add new user to the system
     */
    function membershipCreate()
    {
        // if($this->isAdmin() == TRUE)
        // {
        //     $this->loadThis();
        // }
        // else
        // {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('plan_type','Enter plan type','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('plan_productID','Enter plan product ID','trim|required|xss_clean');
           $this->form_validation->set_rules('plan_description','Enter plan description','trim|required|xss_clean');
           $this->form_validation->set_rules('plan_price','Enter plan price','trim|required|max_length[128]|xss_clean');

           $this->form_validation->set_rules('plan_days','Enter plan days','trim|required|max_length[128]|xss_clean');

           $this->form_validation->set_rules('plan_period_type','Enter plan period','trim|required|xss_clean');           
            
            if($this->form_validation->run() == FALSE) {
                $this->settings();
            } else {
                
                $userInfo['membership_type']        = $this->input->post('plan_type');
                $userInfo['membership_productID']   = $this->input->post('plan_productID');
                $userInfo['membership_description'] = $this->input->post('plan_description');
                $userInfo['membership_price']       = $this->input->post('plan_price');
                $userInfo['membership_tokens']      = $this->input->post('tokens');
                $userInfo['membership_time']        = $this->input->post('plan_days');
                $userInfo['membership_period']      = $this->input->post('plan_period_type');
                $userInfo['membership_created_at']  = date('Y-m-d H:i:s');
               
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('successmsg', 'New Membership Created Successfully');
                }
                else
                {
                    $this->session->set_flashdata('errormsg', 'Failed To Create Membership');
                }
                
                redirect('settings');
            }
       // }
    }  
    

   function updaterange() {

    // if($this->isAdmin() == TRUE) {
    //       $this->loadThis();
    // }
    // else  {

        $this->load->library('form_validation');

           $this->form_validation->set_rules('range_from','Enter Discovery Range From','trim|required|xss_clean');

           $this->form_validation->set_rules('range_to','Enter Discovery Range To','trim|required|xss_clean');

    if($this->form_validation->run() == FALSE)  {
                $this->settings();
     }   else {

            $data['range_from'] = $this->input->post('range_from');
            $data['range_to'] = $this->input->post('range_to');
      
         $result = $this->user_model->update(TB_RANGE, array(), $data);
         //print_r($result);
          if($result) {
              $this->session->set_flashdata('success', 'Discovery Range Updated Successfully'); 
              redirect('settings');       
            } else {
              $this->session->set_flashdata('success', 'Fail To Update'); 
              redirect('settings');    
            }
      }     
        
   // } 
  } 


  function updateping() {

      if($this->input->post('ping_type') == 'limited') {

            $data['ping_limit'] = $this->input->post('ping_limit');
            $data['ping_hr'] = $this->input->post('ping_hr');
            $data['created_at'] = date('Y-m-d H:i:s', time()); 
      
         $result = $this->user_model->update(TB_FREEPING, array(), $data);
      } else {

            $data1['ping_unlimited_hr'] = $this->input->post('ping_hr');
            $data1['start_time'] = date('Y-m-d H:i:s', time());
            $data1['end_time'] = date("Y-m-d H:i:s", strtotime($data1['start_time'] . $data1['ping_unlimited_hr']."hours"));
      
         $result = $this->user_model->update(TB_FREEPING, array(), $data1);
      } 

           if($result) {
              $this->session->set_flashdata('success', 'Pings Limit And Hour Updated Successfully'); 
              redirect('settings');       
           } else {
              $this->session->set_flashdata('success', 'Fail To Update'); 
              redirect('settings');    
           }
  } 


    function ExportCSVToken() {

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');

        $delimiter = ",";
        $newline = "\r\n";
        $filename = "token.csv";
        $query = "SELECT u.user_id , u.user_firstname,u.user_lastname,u.user_gender,u.user_age,m.membership_type,m.membership_price,m.membership_description  FROM `tbl_users` AS u INNER JOIN `tbl_user_purchase` AS p ON u.user_id = p.user_id INNER JOIN `tbl_memberships` AS m ON m.membership_id = p.membership_id where m.membership_type='Token'";
        $result = $this->db->query($query);
        $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
        force_download($filename, $data);

}


  function ExportCSVMonthly() {

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');

        $delimiter = ",";
        $newline = "\r\n";
        $filename = "monthly.csv";
        $query = "SELECT u.user_id , u.user_firstname,u.user_lastname,u.user_gender,u.user_age,m.membership_type,m.membership_price,m.membership_description  FROM `tbl_users` AS u INNER JOIN `tbl_user_purchase` AS p ON u.user_id = p.user_id INNER JOIN `tbl_memberships` AS m ON m.membership_id = p.membership_id where m.membership_type='Monthly'";
        $result = $this->db->query($query);
        $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
        force_download($filename, $data);

}  


  function advertisment() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('subject','Enter subject','trim|required|xss_clean');

        $this->form_validation->set_rules('body_content','Enter message','trim|required|xss_clean');

           if($this->form_validation->run() == FALSE)  {

                $this->promotions();
           }   else {
               
                      if (isset($_FILES['user_profileimage']) && !empty($_FILES['user_profileimage'])) {

                            $uploaddir = './uploads/advertisment/';
                            $file_name = preg_replace("/^(.+?);.*$/", "\\1", $_FILES['user_profileimage']['name']);
                           // $file_name = underscore($_FILES['user_profileimage']['name']);
                             //print_r($_FILES); die();
                            $path = $_FILES['user_profileimage']['name'];
                            $ext = pathinfo($path, PATHINFO_EXTENSION);
                            $user_img = rand() . '_image.' . $ext;
                            $uploadfile = $uploaddir . $user_img;

                          move_uploaded_file($_FILES['user_profileimage']['tmp_name'], $uploadfile);
                              
                            
                             $data  = array(
                                     'ad_title'   => $this->input->post('subject'),
                                     'ad_message' => $this->input->post('body_content'),
                                     'ad_image'   => $user_img
                                    );

                            $res = $this->user_model->insert('tbl_ads', $data);
                            if ($res) {
                               $this->session->set_flashdata('msg', 'Added Successfully');  
                            } else{
                              $this->session->set_flashdata('msg', 'Fail To Add'); 
                            }
                  
                      } 

                      
                 redirect('promotions');    
            } 
  } 
 


    function replyonRequest() {

                $msg = trim($this->input->post('message'));  
                 $data =  array('reply_msg'     => $msg,
                                'reply_status'  => '1'
                               );

                      $replyMsg = $msg;
                      $request_id = $this->input->post('request_id');
            
                      $userId = $this->user_model->select('*', TB_REQUEST, array('request_id' => $request_id));
                      $notify = $this->user_model->select('*', TB_USER, array('user_id' => $userId[0]['user_id']));
      
                    if(count($notify) > 0) {
                        $notdata = array('notdata_user_id'=> $notify[0]['user_id'],
                                         'notdata_device_token' =>$notify[0]['user_deviceid'],
                                         'notdata_type' => 'requestReply'
                                          );
                        $resnot = $this->user_model->insert(TB_NOTDATA, $notdata);  

                        $badgeRows = $this->user_model->select('*', TB_NOTDATA, array('notdata_user_id' => $notify[0]['user_id'], 'notdata_read_status' => 0));

                        $badge = count($badgeRows);   
                         
                        $devicetoken = $notify[0]['user_deviceid'];
                        $message     = $replyMsg;
                        $this->mobilePushNotificationAdmin($devicetoken, $message, 6,$badge,$resnot);

                        $result = $this->user_model->update(TB_REQUEST, array('request_id' =>$request_id), $data);
                        echo json_encode(array('msg'=>'Reply send successfully.','status' =>'1'));
                       
                    } else {
                            echo json_encode(array('msg'=>'User not exists.','status' =>'0')); 
                    }  
    } 


  function getmembershipdata(){
        $data = $this->user_model->select('*', TB_MEMBERSHIPS, array('membership_id' =>$this->input->post('membership_id')));
        //echo "<pre>";print_r($data);die;
         echo json_encode($data[0]);
  } 

  function membership_update() {
        $data = array(
            'membership_type' => $this->input->post('membership_type'),
            'membership_description' => $this->input->post('membership_description'),
            'membership_price' => $this->input->post('membership_price'),
            'membership_period' => $this->input->post('membership_period'),
            'membership_time' => $this->input->post('membership_time')
          );
        $result = $this->user_model->update(TB_MEMBERSHIPS, array('membership_id' =>$this->input->post('membership_id')), $data);
        echo json_encode(array("msg" => 'Membership Data Updated Successfully'));
  }     


    function profile() {

        $this->global['pageTitle'] = 'BondApp : Profile';
        $data['admin'] = $this->user_model->select('*', TB_ADMIN, array('userId' => '1'));
        $this->loadViews("admin/profile", $this->global,$data, NULL);
    }
            
  function profileupdate() {
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'mobile' => $this->input->post('mobile')
          );
        $result = $this->user_model->update(TB_ADMIN, array('userId' =>$this->input->post('id')), $data);

        $admin  = $this->user_model->select('*',TB_ADMIN,array('userId' =>$this->input->post('id')));
        $sessionArray = array(
                            'userId'=>$admin[0]['userId'],                    
                            'role'=>$admin[0]['roleId'],
                            //'roleText'=>$admin[0]['role'],
                            'name'=>$admin[0]['name'],
                            'isLoggedIn' => TRUE
                        );
                        
        $this->session->set_userdata($sessionArray);


          if($result) {
              $this->session->set_flashdata('success', 'Data Update Successfully'); 
          } else {
              $this->session->set_flashdata('error', 'Fail To Update'); 
          }
      redirect('profile');    
  }  


  function changepassword() {

          $old_pass  = $this->input->post('old_pass');
          $new_pass  = $this->input->post('new_pass');
          $confirm_pass  = $this->input->post('confirm_pass');

          $admin  = $this->user_model->select('*',TB_ADMIN,array('userId' =>$this->input->post('id')));

          $pass = $admin[0]['password']; 

            if(verifyHashedPassword($old_pass, $pass)) {
              $data['password'] = getHashedPassword($new_pass);

                  $result = $this->user_model->update(TB_ADMIN, array('userId' =>$this->input->post('id')), $data); 
                $this->session->set_flashdata('successpass', 'Password Updated'); 

            } else {
              $this->session->set_flashdata('errorpass', 'Old Password Is Wrong'); 
            }
        redirect('profile');    
  }  

  function adminsetting() {

        $this->global['pageTitle'] = 'BondApp : Admin Setting';
        $data['set'] = $this->user_model->select('*', TB_ADMINSET, array());
        $this->loadViews("admin/adminsetting", $this->global,$data, NULL);   
  } 


 function adminsettingupdate() {

       $data = array('admin_set_email' => $this->input->post('email'),
                     'admin_set_number' => $this->input->post('contact'),
                     'admin_set_address' => $this->input->post('address')
               );
           $result = $this->user_model->update(TB_ADMINSET, array('admin_set_id' =>'1'), $data); 
               if($result) {
                   $this->session->set_flashdata('success', 'Update Successfully');  
               } else {
                   $this->session->set_flashdata('error', 'Fail to Update');
               }    
        redirect('adminsetting');
  }      

   /**
     * Push notification to all users by device token
     */
  // function pushNotification() {

  //     $message =  $this->input->post('message');
  //     $device  = $this->user_model->getDeviceToken();
  //     $users = count($device);
  //       for($i=1; $i < count($device); $i++) {

  //         $devicetoken = $device[$i]['user_deviceid'];
  //           if($devicetoken != '') {
  //             $notification = $this->mobilePushNotification($devicetoken,$message);
  //           }
  //       }
  //     $this->session->set_flashdata('msg', 'Promotional notification send to '.$users.' users successfully');    
  //     redirect('promotions');
  // }

  function pushNotification() {

      $message =  $this->input->post('message');
      $device  = $this->user_model->getDeviceToken();
      $users = count($device);
        for($i=1; $i < count($device); $i++) {

          $devicetoken = $device[$i]['user_deviceid'];
          $userId      = $device[$i]['user_id'];
            if($devicetoken != '') {
              $notdata = array('notdata_user_id'=> $userId,
                               'notdata_device_token' =>$devicetoken,
                               'notdata_type' => 'promotional'
                              );
              $resnot = $this->user_model->insert(TB_NOTDATA, $notdata);  

              $badgeRows = $this->user_model->select('*', TB_NOTDATA, array('notdata_user_id' => $userId, 'notdata_read_status' => 0));

              $badge = count($badgeRows);   
               
              $devicetoken = $devicetoken;
              $message     = $message;
              $this->mobilePushNotificationAdmin($devicetoken, $message, 5,$badge,$resnot);
            }
        }
      $this->session->set_flashdata('msg', 'Promotional notification send to '.$users.' users successfully');    
      redirect('promotions');
  }


  
 

// NOTIFICATION FUNCTION
  public  function mobilePushNotification($devicetoken,$message) {
    $passphrase = '1234etpl';
      
      $ctx = stream_context_create();
      stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushcert.pem');
      stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
      
      $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
      
      $body['aps'] = array(
        'alert' => array(
          'body' => $message,
          'action-loc-key' => 'BondApp',
        ),
        'badge' => 2,
        'sound' => 'oven.caf',
        );
      // Encode the payload as JSON
      $payload = json_encode($body);
      // Build the binary notification
      $msg = chr(0) . pack('n', 32) . pack('H*', $devicetoken) . pack('n', strlen($payload)) . $payload;
      $result = fwrite($fp, $msg, strlen($msg));
      //socket_close($fp);
      fclose($fp);
      return $result;
  }  

// Notofication for REply on Request

  public  function mobilePushNotificationAdmin($devicetoken, $message, $id,$badge,$resnot) {
    $passphrase = '1234etpl';
      
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushcert.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

    $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    $body['aps'] = array(
      'alert' => array(
        'body' => $message,
        'action-loc-key' => 'BondApp',        
      ),
      'status' => $id,
      'badge' => $badge,
      'notification_read' => $resnot,
      'sound' => 'oven.caf',
    );
    // Encode the payload as JSON
    $payload = json_encode($body);
    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $devicetoken) . pack('n', strlen($payload)) . $payload;
    $result = fwrite($fp, $msg, strlen($msg));
    //socket_close($fp);
    fclose($fp);
    return $result;
}   

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'CodeInsect : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

   
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }




}

?>