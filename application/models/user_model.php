<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, Role.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, Role.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    
    function insert($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("tbl_users");
        $this->db->where("email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_memberships', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('userId, name, email, mobile, roleId');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
		$this->db->where('roleId !=', 1);
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('user_id', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }

    function selectrow_count($sel, $table, $cond = array()) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        $query = $this->db->get();
        $result =  $query->result();
        return $this->db->affected_rows(); 
    } 


    function getUsersAgeRangeFirst()
    {
        $this->db->select('user_id');
        $this->db->from('tbl_users');
        $this->db->where("user_age BETWEEN '18' AND '25'");
        $query = $this->db->get();
        $user = $query->result();
         
            return $this->db->affected_rows(); 
     } 

    function getUsersAgeRangeSecond()
    {
        $this->db->select('user_id');
        $this->db->from('tbl_users');
        $this->db->where("user_age BETWEEN '26' AND '35'");
        $query = $this->db->get();
        $user = $query->result();
         
            return $this->db->affected_rows(); 
     } 

    function getUsersAgeRangeThird()
    {
        $this->db->select('user_id');
        $this->db->from('tbl_users');
        $this->db->where("user_age BETWEEN '36' AND '50'");
        $query = $this->db->get();
        $user = $query->result();
         
            return $this->db->affected_rows(); 
     } 

    function getUsersAgeRangeFourth()
    {
        $this->db->select('user_id');
        $this->db->from('tbl_users');
        $this->db->where("user_age BETWEEN '51' AND '70'");
        $query = $this->db->get();
        $user = $query->result();
         
            return $this->db->affected_rows(); 
     }  

    // function getAllUsers(){
    //     $this->db->select('*');
    //     $this->db->from('tbl_users');
    //     //$this->db->order_by("user_id", "desc");
    //     $query = $this->db->get();
    //  //echo $this->db->last_query();

    //     return $query->result();

    //  }

    function getAllUsers(){
        $query = $this->db->query("Select * from tbl_users order by `user_id` desc");
        //$query = $this->db->get();
        return $query->result_array();    
    }     

    // function getAllUsers(){
    //     $query = $this->db->query("Select @a:=@a+1  as serial_number, t.* from (select @a:=0) initvars, tbl_users as t order by `user_id` desc");
    //     //$query = $this->db->get();
    //     return $query->result_array();    
    // }  

    function getSettings(){
        $query = $this->db->query("Select * from tbl_memberships order by `membership_id` desc");
        //$query = $this->db->get();
        return $query->result_array();    
    }  

   function getDistinctCountry(){
        $this->db->select('DISTINCT(user_country_code),count(`user_id`) as count');
        $this->db->from('tbl_users');
        $this->db->group_by('user_country_code');
        $query = $this->db->get();
        return $query->result();

     }  


    function getCountryCode($code){
        $this->db->select('country_name');
        $this->db->from('tbl_countries');
        $this->db->where("country_code", $code);
        $query = $this->db->get();
        return $query->result_array();

     }


    function delete($table, $where = array()) {
        $this->db->delete($table, $where);
        return $this->db->affected_rows();
    }  

    function update($table, $where = array(), $data) {
        try {
            $this->db->update($table, $data, $where);
            if ($this->db->affected_rows() == 0) {
                return true;
            }
            return $this->db->affected_rows();
        } catch (Exception $e) {
            return false;
        }
    }

    function getpurchse_users() {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->join('tbl_user_purchase','tbl_users.user_id = tbl_user_purchase.user_id');
        $this->db->join('tbl_memberships','tbl_memberships.membership_id = tbl_user_purchase.membership_id');
        $this->db->order_by('tbl_users.user_id','desc');
        $query = $this->db->get();
        return $query->result_array();      
    }


   function select($sel, $table, $cond = array()) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        return $query->result_array();
    } 

    function select_or($sel, $table, $cond = array(), $or = array()) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        foreach ($or AS $k => $v) {
            $this->db->or_where($k, $v);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();
//        die;
        return $query->result_array();
    }

   function selectReplyRequest() {
        $query = $this->db->query("SELECT * FROM `tbl_request` WHERE (`reply_status` ='0' and `request_type` ='Feedback') OR (`reply_status` ='0' and `request_type`='Report Issue') order by request_id desc ");
        //$query = $this->db->get();
        return $query->result_array();      
    }    
    

    function getMembershipMonthly() {
        $this->db->select('distinct(u.user_country_code) ,count(p.user_id) as count');
        $this->db->from('tbl_users u');
        $this->db->join('tbl_user_purchase p ','u.user_id = p.user_id');
        $this->db->join('tbl_memberships m','p.membership_id = m.membership_id');
        $this->db->where('m.membership_type', 'Monthly');
        $this->db->group_by('u.user_country_code');
        $query = $this->db->get();
        return $query->result_array();      
    }

    function getMembershipToken() {
        $this->db->select('distinct(u.user_country_code) ,count(p.user_id) as count');
        $this->db->from('tbl_users u');
        $this->db->join('tbl_user_purchase p ','u.user_id = p.user_id');
        $this->db->join('tbl_memberships m','p.membership_id = m.membership_id');
        $this->db->where('m.membership_type', 'Token');
        $this->db->group_by('u.user_country_code');
        $query = $this->db->get();
        return $query->result_array();      
    } 


    function getPurchaseUsersCount() {
        $this->db->select('user_id');
        $this->db->from('tbl_user_purchase');
        $query = $this->db->get();
        $user = $query->result();
         
            return $this->db->affected_rows(); 
     } 


 function getDistinctGenderToken(){
        $this->db->select('distinct(u.user_gender) ,count(p.user_id) as count');
        $this->db->from('tbl_users u');
        $this->db->join('tbl_user_purchase p ','u.user_id = p.user_id');
        $this->db->join('tbl_memberships m','p.membership_id = m.membership_id');
        $this->db->where('m.membership_type', 'Token');
        $this->db->group_by('u.user_gender');
        $this->db->order_by('user_gender','asc');
       $query = $this->db->get();
       
        return $query->result();

     }       


 function getDistinctGenderMonthly(){
        $this->db->select('distinct(u.user_gender) ,count(p.user_id) as count');
        $this->db->from('tbl_users u');
        $this->db->join('tbl_user_purchase p ','u.user_id = p.user_id');
        $this->db->join('tbl_memberships m','p.membership_id = m.membership_id');
        $this->db->where('m.membership_type', 'Monthly');
        $this->db->group_by('u.user_gender');
        $this->db->order_by('user_gender','asc');
       $query = $this->db->get();
       
        return $query->result();

     }

  function getDeviceToken() {
        $query = $this->db->query("SELECT * FROM `tbl_users` WHERE  user_deviceid != '' ");
        //$query = $this->db->get();
        return $query->result_array();      
    }  

  // function getGraphCount()  {
  //     $query = $this->db->query("SELECT u.`user_created_at` as month , count(*) as total, year(u2.`user_created_at`) as year
  //       from (select distinct month(`user_created_at`) `user_created_at` from `tbl_users`) u
  //       join `tbl_users` u2 on u.`user_created_at` >= month(u2.`user_created_at`)
  //       group by u.`user_created_at` ");
  //     return $query->result_array();    
  // }

  function getGraphCount()  {
      $query = $this->db->query("SELECT MONTH(`user_created_at`) as month, YEAR(`user_created_at`) as year, COUNT(*) as total FROM tbl_users GROUP BY MONTH(`user_created_at`), YEAR(`user_created_at`) ");
      return $query->result_array();    
  }



}

  