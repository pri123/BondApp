<?php

class Common_model extends CI_Model {

    public $table;

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insert($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function insert_batch($table, $data) {
        try {
            $this->db->insert_batch($table, $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    function update($table, $where = array(), $data) {
        try {
            $this->db->update($table, $data, $where);
            if ($this->db->affected_rows() == 0) {
                return true;
            }
            return $this->db->affected_rows();
        } catch (Exception $e) {
            return false;
        }
    }

    function updateIncrement($table, $where = array(), $data) {
        foreach ($data AS $k => $v) {
            $this->db->set($k, $v, FALSE);
        }
        foreach ($where AS $k => $v) {
            $this->db->where($k, $v);
        }
        $this->db->update($table);
        return $this->db->affected_rows();
    }

    function update_batch($table, $where, $data) {
        try {
            $this->db->_protect_identifiers = true;
            $this->db->update_batch($table, $data, $where);
            if ($this->db->affected_rows() == 0) {
                return true;
            }
            return $this->db->affected_rows();
        } catch (Exception $e) {
            return false;
        }
    }

    function delete($table, $where = array()) {
        $this->db->delete($table, $where);
        return $this->db->affected_rows();
    }

    function delete_batch($table, $key, $ids = array()) {
        $this->db->where_in($key, $ids);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }

    function select($sel, $table, $cond = array()) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        return $query->result_array();
    }

    function select_or($sel, $table, $cond = array(), $or = array()) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        foreach ($or AS $k => $v) {
            $this->db->or_where($k, $v);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();
//        die;
        return $query->result_array();
    }

    function selectPage($sel, $table, $cond = array(), $orderBy = array(), $stlimt, $start) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        foreach ($orderBy as $key => $val) {
            $this->db->order_by($key, $val);
        }
        $this->db->limit($stlimt, $start);
        $query = $this->db->get();
        //echo $this->db->last_query(); 
        return $query->result_array();
    }

    function selectQuery($sel, $table, $cond = array(), $orderBy = array(), $join = array(), $joinType = array()) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        foreach ($orderBy as $key => $val) {
            $this->db->order_by($key, $val);
        }
        foreach ($join as $key => $val) {
            if (!empty($joinType) && $joinType[$key] != "") {
                $this->db->join($key, $val, $joinType[$key]);
            } else {
                $this->db->join($key, $val);
            }
        }
        $query = $this->db->get();
        /* echo $this->db->last_query();
          exit; */
        return $query->result_array();
    }

    function select_or_and($sel, $table, $cond = array()) {

        $where = '';
        $this->db->select($sel, FALSE);
        $this->db->from($table);

        $where = "(email like '%" . $cond['search_string'] . "%' or user_name like '%" . $cond['search_string'] . "%' or first_name like '%" . $cond['search_string'] . "%') and country = '" . $cond['country'] . "' and state = '" . $cond['state'] . "'";

        $this->db->where($where);
        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        return $query->result_array();
    }

    function select_in($sel, $table, $cond = array(), $key, $ids) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        $this->db->where("FIND_IN_SET($ids,$key) !=", 0);
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        return $query->result_array();
    }


    function searchUserByName($lat, $long, $miles,$search,$user_id,$range_min,$range_max) {
         $query = $this->db->query('SELECT *, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians(`user_lat`) ) * cos( radians(`user_long`) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians(`user_lat`) ) ) ) AS distance FROM tbl_users where user_firstname LIKE "%'.$search.'%" and user_id != '.$user_id.' and user_loginstatus = 1 and (`user_age` between '.$range_min.' and '.$range_max.') HAVING distance < '.$miles.'');
           
            // echo $this->db->last_query(); die;
            return $query->result_array();
    }

    function searchUserByNamePre($lat, $long, $miles,$search,$user_id,$searchPre,$range_min,$range_max) {
         $query = $this->db->query('SELECT *, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians(`user_lat`) ) * cos( radians(`user_long`) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians(`user_lat`) ) ) ) AS distance FROM tbl_users where user_firstname LIKE "%'.$search.'%" and user_id != '.$user_id.' and user_loginstatus = 1 and user_gender = "'.$searchPre.'" and (`user_age` between '.$range_min.' and '.$range_max.') HAVING distance < '.$miles.'');
           
            // echo $this->db->last_query(); die;
            return $query->result_array();
    }


// search online users by particular age range with preferences 
    function usersByRangeDataOnline($lat, $long, $miles,$user_id,$subscription,$range_min,$range_max,$search) {
        $query = $this->db->query('SELECT IFNULL(user_id,"") as user_id,IFNULL(user_profileimage,"") as profileimage, IFNULL(user_firstname,"") as firstname, IFNULL(user_lastname,"") as lastname, IFNULL(user_deviceid,"") as deviceid, IFNULL(user_age,"") as age, IFNULL(user_lat,"") as user_lat, IFNULL(user_long,"") as user_long, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians(`user_lat`) ) * cos( radians(`user_long`) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians(`user_lat`) ) ) ) AS distance FROM tbl_users where `user_onlinestatus` = 1 and `user_subscription`= '.$subscription.' and `user_loginstatus` = 1 and user_id!='.$user_id.' and user_gender = "'.$search.'" and (`user_age` between '.$range_min.' and '.$range_max.') HAVING distance < '.$miles.'');
        //echo $this->db->last_query(); die;
        return $query->result_array();
    } 

// search offline and online users by particular age range with preferences
    function usersByRangeDataOffline($lat, $long, $miles,$user_id,$range_min,$range_max,$search) {
        $query = $this->db->query('SELECT IFNULL(user_id,"") as user_id,IFNULL(user_profileimage,"") as profileimage, IFNULL(user_firstname,"") as firstname, IFNULL(user_lastname,"") as lastname, IFNULL(user_deviceid,"") as deviceid, IFNULL(user_age,"") as age, IFNULL(user_lat,"") as user_lat, IFNULL(user_long,"") as user_long, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians(`user_lat`) ) * cos( radians(`user_long`) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians(`user_lat`) ) ) ) AS distance FROM tbl_users where `user_loginstatus` = 1 and user_id!='.$user_id.' and user_gender = "'.$search.'" and (`user_age` between '.$range_min.' and '.$range_max.') HAVING distance < '.$miles.'');
        //echo $this->db->last_query(); die;
        return $query->result_array();
    }

// search online users by particular age range
    function usersByRangeOnline($lat, $long, $miles,$user_id,$subscription,$range_min,$range_max) {
        $query = $this->db->query('SELECT IFNULL(user_id,"") as user_id,IFNULL(user_profileimage,"") as profileimage, IFNULL(user_firstname,"") as firstname, IFNULL(user_lastname,"") as lastname, IFNULL(user_deviceid,"") as deviceid, IFNULL(user_age,"") as age, IFNULL(user_lat,"") as user_lat, IFNULL(user_long,"") as user_long, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians(`user_lat`) ) * cos( radians(`user_long`) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians(`user_lat`) ) ) ) AS distance FROM tbl_users where `user_onlinestatus` = 1 and `user_subscription`= '.$subscription.' and `user_loginstatus` = 1 and user_id!='.$user_id.' and (`user_age` between '.$range_min.' and '.$range_max.') HAVING distance < '.$miles.'');
        //echo $this->db->last_query(); die;
        return $query->result_array();
    }     

// search offline and online users by particular age range with preferences
    function usersByRangeOffline($lat, $long, $miles,$user_id,$range_min,$range_max) {
        $query = $this->db->query('SELECT IFNULL(user_id,"") as user_id,IFNULL(user_profileimage,"") as profileimage, IFNULL(user_firstname,"") as firstname, IFNULL(user_lastname,"") as lastname, IFNULL(user_deviceid,"") as deviceid, IFNULL(user_age,"") as age, IFNULL(user_lat,"") as user_lat, IFNULL(user_long,"") as user_long, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians(`user_lat`) ) * cos( radians(`user_long`) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians(`user_lat`) ) ) ) AS distance FROM tbl_users where `user_loginstatus` = 1 and user_id!='.$user_id.' and (`user_age` between '.$range_min.' and '.$range_max.') HAVING distance < '.$miles.'');
        //echo $this->db->last_query(); die;
        return $query->result_array();
    }                       


    function select_or_and_query($sel,$table,$cond,$or_cond) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        $this->db->where($or_cond, NULL, FALSE);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function select_first($sel, $table, $cond = array()) {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v) {
            $this->db->where($k, $v);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        return $query->row();
    }

    function getUserCheck($sender,$todaytime) {
        $query = $this->db->query('SELECT * from `tbl_userping` where `userping_user_id` = "'.$sender.'" and  `created_at` < "'.$todaytime.'" AND `end_time` > "'.$todaytime.'"  ');
        return $query->result_array();
    } 

    function getChatList($user_id) {
        $query = $this->db->query('SELECT * from `tbl_ping` where (`sender_id` = "'.$user_id.'" and  `ping_accept_status` = 2) or (`receiver_id` = "'.$user_id.'" and `ping_accept_status` = 2) ');
        return $query->result_array();
    }  

    function getUserPingExists($sender,$receiver) {
        $query = $this->db->query('SELECT * from `tbl_ping` where (`sender_id` = "'.$sender.'" and  `receiver_id` = "'.$receiver.'") or (`sender_id` = "'.$receiver.'" and `receiver_id` = "'.$sender.'") ');
        return $query->result_array();
    }         

    function selectChat($sender,$receiver){
        $query = $this->db->query('SELECT * FROM `tbl_chat` WHERE (`sender_id` = "'.$sender.'" AND  `receiver_id` = "'.$receiver.'") OR (`sender_id` = "'.$receiver.'" AND  `receiver_id` = "'.$sender.'") ');
        //$query = $this->db->query('SELECT * FROM `tbl_chat` WHERE `sender_id` = "'.$sender.'" AND  `receiver_id` = "'.$receiver.'" ');
        return $query->result_array();
    }

    function selectChatReceiver($sender,$receiver){
        $query = $this->db->query('SELECT * FROM `tbl_chat` WHERE `sender_id` = "'.$receiver.'" AND  `receiver_id` = "'.$sender.'" ');
        //$query = $this->db->query('SELECT * FROM `tbl_chat` WHERE `sender_id` = "'.$sender.'" AND  `receiver_id` = "'.$receiver.'" ');
        return $query->result_array();
    }    

    function chatMsgDelete($newtime) {
        $query = $this->db->query('DELETE from `tbl_chat` where `msg_status` = 0 and `created_at` <= "'.$newtime.'" ');
    }

    function notifreceiveUnread($userid) {
        $query = $this->db->query('SELECT * from `tbl_ping`  where `receiver_id` = "'.$userid.'" ORDER BY `ping_receive_at` DESC ');
        return $query->result_array();
    } 

    function notifaccept($userid) {
        $query = $this->db->query('SELECT * from `tbl_ping`  where `sender_id` = "'.$userid.'" and `ping_accept_status` = 2 ORDER BY `ping_accept_at` DESC ');
        return $query->result_array();
    }   

    function selectChatList($userid){
        $query = $this->db->query('SELECT * FROM `tbl_ping` WHERE (`sender_id` = "'.$userid.'" AND  `ping_accept_status` = 2) OR (`receiver_id` = "'.$userid.'" AND  `ping_accept_status` =2) ORDER BY `ping_update_at` DESC ');
        return $query->result_array();
    }

    function listMembership(){
        $query = $this->db->query('SELECT * FROM `tbl_memberships` ORDER BY `membership_type` DESC ');
        return $query->result_array();
    }

    function selectChatUsers($sender){
        $query = $this->db->query('SELECT * FROM `tbl_ping` WHERE (`sender_id` = "'.$sender.'" AND `ping_accept_status` = 2 ) OR  (`receiver_id` = "'.$sender.'" AND `ping_accept_status` = 2) ');
        return $query->result_array();
    }

    function checkMobileExists($number,$user_id){
        $query = $this->db->query('SELECT * FROM `tbl_users` WHERE `user_mobileno` = "'.$number.'" AND `user_id` != "'.$user_id.'" ');
         return $query->result_array();
    } 

    function getFreePingsData(){
        $query = $this->db->query('SELECT *,date(start_time) as startdate, date(end_time) as enddate FROM `tbl_free_ping` ');
        return $query->result_array();
    } 


}

?>
