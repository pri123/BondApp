<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $pageTitle; ?></title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.jpg">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url(); ?>vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap/dist/css/custom.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/development.css" rel="stylesheet">
 <!--Datatables -->
    <link href="<?php echo base_url(); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">    
    <link href="<?php echo base_url(); ?>assests/datatables/css/dataTables.bootstrap.css" rel="stylesheet"> 

	<script>
    //ankush
    var start = new Date();
    jQuery(".heart-loader").show();
  </script> 
  </head>

  <body class="nav-md">
		<div class="heart-loader" style="background-color: rgb(255, 255, 255);">
			<div id="heart"></div>
		</div>
    <div class="container body" >
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; background-color: #ff3b3b;">
              <a href="<?php echo base_url('dashboard'); ?>" class="site_title"><img src="<?php echo base_url('assets/images/Icon-29.jpg'); ?>"> <span>BondApp!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <!--<img src="<?php echo base_url();?>img/img.jpg" alt="..." class="img-circle profile_img">-->
              </div>
              <div class="profile_info">
                <span>Welcome, <?php echo $name; ?></span>
                <!--<h2><?php //echo $name; ?> </h2>-->
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                  </li>
                  
                  <li><a href="<?php echo base_url();?>userManagement"><i class="fa fa-users"></i> User Management</a>
                  </li>

                  <li><a href="<?php echo base_url();?>allRequest"><i class="fa fa-edit"></i> All Request</a>
                  </li>

                  <li><a href="<?php echo base_url();?>financialManagement"><i class="fa fa-money"></i> Financial Management</a>
                  </li>

                  <li><a href="<?php echo base_url();?>settings"><i class="fa fa-cog"></i> Settings</a>
                  </li>

                  <li><a href="<?php echo base_url();?>promotions"><i class="fa fa-tags"></i> Promotions</a>
                  </li>

                  <li><a href="<?php echo base_url();?>statistics"><i class="fa fa-bar-chart-o"></i> Statistics</a>
                  </li>                                                      

                </ul>
              </div>              
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings" href="<?php echo base_url(); ?>adminsetting">
                <spa class="glyphicon glyphicon-cog"  aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Profile" href="<?php echo base_url(); ?>profile">
                <span  class="glyphicon glyphicon-user" aria-hidden="true"></span>
              </a>
              
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url(); ?>logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

               <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>img/img.jpg" alt=""><?php echo $name; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user"></i> Profile</a></li>
                    <li>
                      <a href="<?php echo base_url(); ?>adminsetting"><i class="fa fa-cog"></i> Settings</a>
                    </li>
                    <li>
						<a href="<?php echo base_url(); ?>logout" class=""><i class="fa fa-sign-out"></i> Sign out</a>
					 </li>
                  </ul>
                </li>                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->