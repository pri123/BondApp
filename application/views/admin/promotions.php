 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>
           $(function() {
            $(".hide-it").hide(5000);
           });
        </script>

<script>
 function change_tab(id)
 {
   document.getElementById("page_content").innerHTML=document.getElementById(id+"_desc").innerHTML;
   document.getElementById("page1").className="notselected";
   document.getElementById("page2").className="notselected";
   document.getElementById("page3").className="notselected";
   document.getElementById("page4").className="notselected";
   document.getElementById(id).className="selected";
 }
</script>

<style>
#main_content
{
 margin-top:50px;
 
}
#main_content li
{
 display:inline;
 list-style-type:none;
 padding:14px;
 border-radius:5px 5px 0px 0px;
 color:#292A0A;
 font-weight:bold;
 cursor:pointer;
}
#main_content li.notselected
{
 background-color:#BFC9CA;
 color:#292A0A; 
}
#main_content li.selected
{
 background-color:#797D7F;
 color:#292A0A; 
}
#main_content .hidden_desc
{
 display:none;
 visibility:hidden;
}
#main_content #page_content
{
 padding:10px;
 margin-top:9px;
 border-radius:0px 5px 5px 5px;
 color:#2E2E2E;
 line-height: 1.6em;
 word-spacing:4px;
}
</style>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="clearfix"></div>

            <div class="row promotion" style="min-height: 530px;">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Promotion <!-- <small>Users</small> --></h2>                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content"> 

                      <?php if($this->session->flashdata('msg')){  ?>
                      <h4 class="hide-it" style="margin: 0 0 5px 0; padding: 8px; background-color: #00a65a; color:white;">
                      <?php echo $this->session->flashdata('msg'); } ?>
                      </h4>

                  <link rel="stylesheet" type="text/css" href="tabs_style.css">

<div id="main_content">

 <!-- <li class="selected" id="page1" onclick="change_tab(this.id);">Template</li>
 <li class="notselected" id="page2" onclick="change_tab(this.id);">Recipients</li>
 <li class="notselected" id="page3" onclick="change_tab(this.id);">Cycle</li> 
 <li class="selected" id="page4" onclick="change_tab(this.id);">Push Notifications</li>
 
 <!-- <div class='hidden_desc' id="page1_desc">
     <div class="col-md-12 col-sm-12 col-xs-12">
       <form name="form" method="post" action="<?php echo base_url(); ?>advertisment" enctype="multipart/form-data">
         <input type="text" name="subject" class="form-control" id="subject" placeholder="Subject">
        
         <span class="col-md-3 col-sm-3"></span><br>
         <textarea type="text" name="body_content" rows=4 class="form-control" id="body_content" placeholder="Message"></textarea>
         <br>
         <input type="file" class="form-control" name="user_profileimage" id="user_profileimage">
         <br>
         <input type="submit" class="btn btn-info" value="Send">
      </form>
    </div>
 </div>

 <div class='hidden_desc' id="page2_desc">
  
  Hello this is Page 2 description and this is just a sample text .This is the demo of Multiple Tab In Single Page Using JavaScript and CSS.
 </div>
 
 <div class='hidden_desc' id="page3_desc">
  
  Hello this is Page 3 description and this is just a sample text .This is the demo of Multiple Tab In Single Page Using JavaScript and CSS. 
  Hello this is Page 3 description and this is just a sample text .This is the demo of Multiple Tab In Single Page Using JavaScript and CSS.
 </div> -->

 <div class='hidden_desc' id="page4_desc">
      <div class="col-md-12 col-sm-12 col-xs-12">
      <form name="form" method="post" action="<?php echo base_url(); ?>pushNotification">
            <br>
             <textarea type="text" name="message" rows=6 class="form-control" id="message" required="required"></textarea>
             <br>
             <input type="hidden" name="X-API-KEY" value="12rwr23">
             <input type="submit" class="btn btn-info" value="Send">
      </form>
    </div>
 </div>
 
 <div id="page_content" class="col-md-10 col-sm-10 col-xs-12" >

    <div class="col-md-12 col-sm-12 col-xs-12">
      <form name="form" method="post" action="<?php echo base_url(); ?>pushNotification">
            <br>
             <label>Push Notification</label>
             <textarea type="text" name="message" rows=6 class="form-control" id="message" required="required"></textarea>
             <br>
             <input type="hidden" name="X-API-KEY" value="12rwr23">
             <input type="submit" class="btn btn-info" value="Send">
      </form>
    </div>
 </div>
 
</div>                  
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- </div> -->
        <!-- /page content -->

      </div>
    </div>

     <!-- jQuery -->
    <script src="<?php echo base_url();?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>

  </body>
</html>
