
       
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Financial Management</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row" style="min-height: 500px;">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Purchase Monitoring </h2>                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Sr No.</th>
                          <th>Name</th>
                          <th>Mobile number</th>
                          <th>Subscription</th>
                          <th>Price</th>
                          <th>Purchase Date</th>
                          <th>Login Status</th>
                          <th>Country</th>
                        </tr>
                      </thead>


                      <tbody>
                  <?php $i=1; foreach($UsersPurchasedata as $key) { ?>    
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $key['user_firstname']." ".substr($key['user_lastname'],0,1); ?></td>
                          <td><?php echo $key['user_mobileno']; ?></td>
                          
                          
                          <td><?php echo $key['membership_description']; ?></td>
                          <td><?php echo "$ ".$key['membership_price']; ?></td>
                          <td><?php echo $key['user_created_at']; ?></td>
                          <td><?php if($key['user_status'] == 0) { echo "Active"; } else { echo "Inactive"; } ?></td>
                          <td><?php echo countryCode($key['user_country_code']); ?></td>
                         
                        </tr>
                  <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Payment Issues</h2>                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive">                   

                         <table id="table_id" class="table table-striped table-bordered">
                            <thead>
                        <tr>
                          <th>Sr No.</th>
                          <th>Name</th>
                          <th>Email /Mobile</th>
                          <th>Message</th>
                          <th>Request</th>
                          <th>Date</th>
                          <th>Action</th>
                                               
                        </tr>
                      </thead>


                      <tbody>
                    <?php $i=1;  foreach($PurchaseIssue as $key) {  
                             $datetime = explode(" ",$key['created_at']);
                             $date = $datetime[0];  
                    ?>          
                        <tr>
                          <td><?php echo $i--; ?></td>
                          <td><?php echo $key['name']; ?></td>
                          <td><?php echo $key['email']; ?></td>
                          <td><?php echo $key['message']; ?></td>
                          <td><?php echo $key['request_type']; ?></td>
                          <td style="white-space: nowrap;"><?php echo $date; ?></td>
                          <td style="white-space: nowrap;">
                          <button class="btn btn-warning" onclick="reply(<?php echo $key['request_id'];?>)"><i class="glyphicon glyphicon-pencil"></i>
                            </button>
                          
                         
                          </td>
                          
                          
     
                        </tr>
                  <?php } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>     -->        
            </div>
          </div>
        </div>
        <!-- /page content -->

 <script src="<?php echo base_url()?>assests/jquery/jquery-3.1.0.min.js"></script>
  <script src="<?php echo base_url()?>assests/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>assests/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url()?>assests/datatables/js/dataTables.bootstrap.js"></script>


  <script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
  
    function reply(id)  {
      $('#form')[0].reset();
       document.getElementById("request_id").value =id;
       //request_id = id;
       $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
       $('.modal-title').text('Reply On Request'); // Set title to Bootstrap modal title
   
    }



    function save() {
      // ajax adding data to database
          $.ajax({
            url : "<?php echo base_url()?>replyonRequest",
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
              if(data.status == 1) {
                 document.getElementById('btnSave').disabled = true;
                 document.getElementById('btnCancel').disabled = true;
                  $('#response').html(data.msg);
                  $('#response').show();
                  $('#modal_form #form').hide();
                  setTimeout(function(){location.reload();},800);
              } else {
                  $('#ResId').html(data.msg);
                  $('#ResId').show();
                  $('#modal_form #form').hide();
              }
            }
        });
    }


  </script>

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Reply</h3>
        <h4 id="response" style="display:none; margin: 0 0 5px 0; padding: 8px; background-color: #00a65a; color:white;"></h4>
        <h4 id="ResId" style="display:none; margin: 0 0 5px 0; padding: 8px; background-color: red; color:white;"></h4>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" name="request_id" id="request_id" value="">
          <div class="form-body">
            <!--<div class="form-group">
              <label class="control-label col-md-3">Subject</label>
              <div class="col-md-9">
                <input name="subject" placeholder="Subject" class="form-control" type="text">
              </div>-->
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Message</label>
              <div class="col-md-9">
                <textarea name="message" row="10" cols="10" style="resize: none; height: 150px;" placeholder="Message" class="form-control" type="text"></textarea>
              </div>
            </div>
           
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Reply</button>
            <button type="button" id="btnCancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
       
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>

  </body>
</html>
