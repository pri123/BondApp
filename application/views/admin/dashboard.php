 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>     
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
              <div class="count"><?php print($userArr); ?></div>
              <span class="count_bottom"></span>
            </div>

          </div>
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Total Sign Ups</small></h3>
                  </div>
                  <div class="col-md-6"> </div>
                </div>
                <!-- <div id="chart_plot_01" class="demo-placeholder"></div> -->
                <div id="chart_plot_001" class="demo-placeholder"></div>
                 
          </div>
          <br />

          <div class="row">


            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Top Regions</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>-->
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Traffic Source</th>
                          <th>View Status</th>
                          
                      </thead>


                      <tbody>
                      <?php //print_r($DistinctCountry); ?>
                      <?php foreach ($DistinctCountry as $key) {  ?>
                        <tr>
                          <td><?php echo countryCode($key->user_country_code) ?></td>
                          <td><?php echo round($key->count / $userArr *100,2)." %" ; ?></td>
                        </tr>
                      <?php } ?>  
                      </tbody>
                    </table>
                
                </div>
              </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 bg-white">
               <div class="x_panel tile fixed_height_320">
                  <div class="x_title">
                    <h2>User Age Group</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>18 - 25</p>
                      <div class="">
                        <div class="progress progress_sm" style="width:<?php echo $RangeFirst; ?>;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="80"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>25 - 35</p>
                      <div class="">
                        <div class="progress progress_sm" style="width:<?php echo $RangeSecond; ?>;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="60"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>35 - 50</p>
                      <div class="">
                        <div class="progress progress_sm" style="width:<?php echo $RangeThird; ?>;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>50 - 70</p>
                      <div class="">
                        <div class="progress progress_sm" style="width:<?php echo $RangeFourth; ?>;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                </div>

                <div class="clearfix"></div>
              </div>
            </div>



           
          </div>

        
      </div>
    </div>
<!-- </body> -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url(); ?>vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url(); ?>vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url(); ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url(); ?>vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url(); ?>vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url(); ?>vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url(); ?>vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url(); ?>vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url(); ?>vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url(); ?>vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>vendors/jquery/dist/custom.min.js"></script>
	
  
<!-- </html> -->

<script>
  init_flot_chart1();
  function init_flot_chart1(){
    var year = '<?php echo $graph; ?>';
    // alert(year);
    // console.log(year);
    
    if( typeof ($.plot) === 'undefined'){ return; }
    
    var arr_data2 = [ <?php echo $graph; ?>
      // [gd(2018, 1, 1), 82],
      // [gd(2018, 1, 2), 23],
      // [gd(2018, 1, 3), 66],
      // [gd(2018, 1, 4), 9],
      // [gd(2018, 2, 5), 119],
      // [gd(2018, 2, 6), 6],
      // [gd(2018, 3, 7), 9],
    ];
    
    
    
    var chart_plot_001_settings = {
          series: {
            lines: {
              show: false,
              fill: true
            },
            splines: {
              show: true,
              tension: 0.4,
              lineWidth: 1,
              fill: 0.4
            },
            points: {
              radius: 0,
              show: true
            },
            shadowSize: 2
          },
          grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
          },
          colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
          xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "month"],
            // minTickSize: [1, "month"],
            // tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
          },
          yaxis: {
            // ticks: 10 ,
            tickColor: "rgba(51, 51, 51, 0.06)",
          },
          tooltip: false
        }
          
    
        if ($("#chart_plot_001").length){
          jQuery.plot( $("#chart_plot_001"), [ arr_data2 ],  chart_plot_001_settings );
        }
  } 
</script>
