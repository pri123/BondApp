<?php
      $today = date('Y-m-d H:i:s'); 
      $end = $ping[0]['end_time'];
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"> </script>
<script>
   $(function() {
    $(".hide-it").hide(3000);
   });
</script>
<script>
$(document).ready(function(){
  var today = '<?php echo $today; ?>';
  var end = '<?php echo $end; ?>';
    if(today < end){
        $('#unlimitdiv').show();
        $('#limitdiv').hide();
    } else {
        $('#unlimitdiv').hide();
        $('#limitdiv').show();
    }
});
$(document).ready(function(){
      $('#ping_type').on('change', function() {
        if ( this.value == 'unlimited') {
          $("#limit").hide();
        }
        else {
          $("#limit").show();
        }
      });
});
</script>


<script type="text/javascript">
function showdv(obj,m,t)
{
txt=obj.options[obj.selectedIndex].text; 
document.getElementById("total_tokens").style.display='none';
if(txt.match(m))
{
document.getElementById("total_tokens").style.display='none';
}
if(txt.match(t))
{
document.getElementById("total_tokens").style.display='block';
}
}
</script>

        <!-- page content -->
     <div class="right_col" role="main">
          <div class="page-title">
              <div class="title_left">
                <h3>Settings</h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                   <?php if($this->session->flashdata('success')){  ?>
                      <h4 class="hide-it col-md-6" style="margin: 0 0 5px 0; padding: 8px; background-color: #00a65a; color:white;">
                          <?php echo $this->session->flashdata('success'); } ?>
                      </h4>
                   <?php if($this->session->flashdata('error')){  ?>
                      <h4 class="hide-it col-md-6" style="margin: 0 0 5px 0; padding: 8px; background-color: red; color:white;">
                       <?php echo $this->session->flashdata('error'); } ?>
                      </h4>   
            </div>        
           <div class="row settingpage">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Discovery Range (Feet)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    
                     <form method="post" action="<?php echo base_url() ?>updaterange" data-parsley-validate class="form-horizontal form-label-left">
                  
                     <?php // print_r($range); 
                       foreach ($range as $key) { ?>

                      <div class="form-group">
                        <label class="control-label">Range From
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="range_from" required="required" class="form-control" name="range_from" value="<?php echo $key['range_from']; ?>" readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Range To
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="number" id="range_to" name="range_to" required="required" class="form-control" value="<?php echo $key['range_to']; ?>" min="100">
                        </div>
                      </div>
 
                    
                      <div class="form-group">
                        <label class="control-label"></label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          
                          <input type="submit" class="btn btn-success" value="Update">
                        </div>
                      </div>
                    <?php } ?>

                    </form>
                  </div>
                </div>
              </div>

             <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Pings Management</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    
                     <form method="post" action="<?php echo base_url() ?>updateping" data-parsley-validate class="form-horizontal form-label-left" name="form">

                     
                    <?php  foreach ($ping as $key) {  ?>

                    <div id="limitdiv" style="display:none;"> 
                       <div class="form-group">
                        <label class="control-label">Ping Type
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="ping_type" id="ping_type" class="form-control" required="required" disabled>
                              <option value="">Please Select</option>
                              <option value="limited"<?php if ($key['ping_type'] == 'limited') echo ' selected="selected"'; ?>>Limited</option>
                              <option value="unlimited"<?php if ($key['ping_type'] == 'unlimited') echo ' selected="selected"'; ?>>Unlimited</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Ping Hour
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="number" id="ping_hr" name="ping_hr" required="required" class="form-control" value="<?php echo $key['ping_hr']; ?>" min="1" disabled>
                        </div>
                      </div>

                      <div class="form-group" id="limit">
                        <label class="control-label">Ping Limit
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="number" id="ping_limit" required="required" class="form-control" name="ping_limit" value="<?php echo $key['ping_limit']; ?>" min="1" disabled>
                        </div>
                      </div> 
                    

                      <div class="form-group">
                        <label class="control-label">
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <span class="btn btn-primary" id="edit" onclick="enableButton2()">Edit</span>
                          <input type="submit" class="btn btn-success" value="Update" id="update" disabled>
                        </div>
                      </div>
                    </div>  
                  

                    <div id="unlimitdiv" style="display:none;">
                       <div class="form-group">
                        <label class="control-label">Ping Type
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input name="unlimited" type="text" class="form-control" value="Unlimited" disabled>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Ping Hour
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="number" id="ping_hr" name="ping_hr" required="required" class="form-control" value="<?php  echo $key['ping_unlimited_hr']; ?>" min="1" disabled>
                        </div>
                      </div>
                    
                      <div class="form-group">
                        <label class="control-label">
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <button class="btn btn-primary" disabled>Edit</button>
                          <input type="submit" class="btn btn-success" value="Update" disabled>
                        </div>
                      </div>
                    </div>  
                      
                    <?php } ?>

                    </form>
                  </div>
                </div>
              </div>              
            </div> 


           <div class="row memplan">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Membership Plans</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>-->
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                   <button class="btn btn-info" id="buttonform">Add Membership</button> 

                  <div class="x_content">

                  <div class="col-md-12">
                   <?php if($this->session->flashdata('successmsg')){  ?>
                      <h4 class="hide-it col-md-6" style="margin: 0 0 5px 0; padding: 8px; background-color: #00a65a; color:white;">
                          <?php echo $this->session->flashdata('successmsg'); } ?>
                      </h4>
                   <?php if($this->session->flashdata('errormsg')){  ?>
                      <h4 class="hide-it col-md-6" style="margin: 0 0 5px 0; padding: 8px; background-color: red; color:white;">
                       <?php echo $this->session->flashdata('errormsg'); } ?>
                      </h4>   
                  </div>

                    <form style="visibility:hidden; display:none;" name="formadd" id="formadd" method="post" onsubmit="return validateForm();" action="<?php echo base_url() ?>membershipCreate"  data-parsley-validate class="form-horizontal form-label-left" >
					<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <div class="form-group">
                        <label class="control-label">Membership type
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select id="plan_type" class="form-control col-md-7 col-xs-12" name="plan_type" id="plan_type" onchange="showdv(this,'Monthly','Token');">
                             <option value="">Select Membership type</option>
                             <option value="Monthly">Monthly</option>
                             <option value="Token">Token</option>

                          </select>
                        </div>
                        <div class="col-md-12"></div>
                        <div id="plan_type_errorbox" class="col-md-12"></div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Membership Product ID
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="plan_productID" name="plan_productID" class="form-control col-md-7 col-xs-12" ></textarea>
                        </div>
                        <div class="col-md-12"></div>
                        <div id="plan_productID_errorbox" class="col-md-12"></div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Membership Description
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea id="plan_description" row="20"  name="plan_description" class="form-control col-md-7 col-xs-12" ></textarea>
                        </div>
                        <div class="col-md-12"></div>
                        <div id="plan_description_errorbox" class="col-md-12"></div>
                      </div>
 
                      <div class="form-group">
                        <label class="control-label">Membership Price
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="plan_price" name="plan_price" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="plan_price_errorbox" class="col-md-12"></div>
                      </div>

                      <div class="form-group" id="total_tokens" style="display:none;">
                        <label class="control-label">Tokens Count</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="tokens" name="tokens" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Membership Period
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="plan_period_type" id="plan_period_type" class="form-control col-md-7 col-xs-12">
                             <option value="">Select Membership Period</option>
                             <option value="min">Minutes</option>
                             <option value="hr">Hours</option>
                             <option value="day">Days</option>
                             <!--<option value="month">Months</option>-->
                          </select>
                        </div>
                        <div class="col-md-12"></div>
                        <div id="plan_period_type_errorbox" class="col-md-12"></div>
                      </div>                      

                      <div class="form-group">
                        <label class="control-label">Membership Time
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="plan_days" name="plan_days" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="plan_days_errorbox" class="col-md-12"></div>
                      </div>

                     <div class="form-group">
                        <label class="control-label">
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="submit" class="btn btn-success" value="Submit">
                          <button class="btn btn-info" id="cancelForm">Cancel</button>
                        </div>
                      </div>
                      </div>
                      </div>

                    </form>
                    <br>

                   <table id="table_id" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Sr No.</th>
                          <th>Membership Type</th>
                          <th>Membership Product ID</th>
                          <th>Membership Description</th>
                          <th>Membership Price</th>
                          <th>Membership Plan Period</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                     
                     <?php $i=1; foreach ($membership as $key) {  ?>
                      
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $key['membership_type']; ?></td>
                          <td><?php echo $key['membership_productID']; ?></td>
                          <td><?php echo $key['membership_description']; ?></td>
                          <td><?php echo "$ ".$key['membership_price']; ?></td>
                          <?php  if($key['membership_period'] == 'min') { 

                                    $key['membership_period'] = 'Minutes';
                                } else if($key['membership_period'] == 'hr') {
                                    $key['membership_period'] = 'Hours';
                                } else if($key['membership_period'] == 'day') {
                                    $key['membership_period'] = 'Days';
                                } else if($key['membership_period'] == 'month') {
                                    $key['membership_period'] = 'Months';
                                }

                          ?>

                          <td><?php echo $key['membership_time'] ." ". $key['membership_period']; ?></td>
                          <td>
                          
                         <button class="btn btn-warning" onclick="edit(<?php echo $key['membership_id'];?>)"><i class="glyphicon glyphicon-pencil"></i>
                         </button>
                          </td>
                        </tr>
                     <?php } ?>
                      </tbody>
                    </table>                    
                  </div>
                </div>
              </div>
            </div>
     </div>
        <!-- </div> -->
        <!-- /page content -->

<script>

function enableButton2() {

    document.getElementById("update").disabled = false;
    document.getElementById("ping_type").disabled = false;
    document.getElementById("ping_limit").disabled = false;
    document.getElementById("ping_hr").disabled = false;
    document.getElementById("edit").disabled = true;
}
</script>

 <script src="<?php echo base_url()?>assests/jquery/jquery-3.1.0.min.js"></script>
  <script src="<?php echo base_url()?>assests/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>assests/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url()?>assests/datatables/js/dataTables.bootstrap.js"></script>


  <script type="text/javascript">

    var theButton = document.getElementById('buttonform');

    theButton.onclick = function() { 
        document.getElementById('formadd').style.visibility='visible';  
        document.getElementById('formadd').style.display='block';
        document.getElementById('buttonform').style.display='none';   
    }

    var Cancelbutton = document.getElementById('cancelForm');
    Cancelbutton.onclick = function() { 
        document.getElementById('formadd').style.display='none';
        document.getElementById('buttonform').style.display='block';   
    } 
  </script>


<script type="text/javascript">

function validateForm() {
    var plan_type = document.forms["formadd"]["plan_type"].value;
    if (plan_type == "") {
        document.getElementById('plan_type_errorbox').style.color='red';
        document.getElementById('plan_type_errorbox').innerHTML ="Enter Membership Type";
        formadd.plan_type.focus();
        return false;
    }
    var plan_productID = document.forms["formadd"]["plan_productID"].value;
    if (plan_productID == "") {
        document.getElementById('plan_productID_errorbox').style.color='red';
        document.getElementById('plan_productID_errorbox').innerHTML ="Enter Membership Product ID";
        formadd.plan_productID.focus();
        return false;
    }
    var plan_description = document.forms["formadd"]["plan_description"].value;
    if (plan_description == "") {
        document.getElementById('plan_description_errorbox').style.color='red';
        document.getElementById('plan_description_errorbox').innerHTML ="Enter Membership Description";
        formadd.plan_description.focus();
        return false;
    }
    var plan_price = document.forms["formadd"]["plan_price"].value;
    var reg                    = /^[0-9]+([,.][0-9]+)?$/g;
    if (!plan_price.match(reg)) {
        document.getElementById('plan_price_errorbox').style.color='red';
        document.getElementById('plan_price_errorbox').innerHTML ="Enter Valid Membership Price";
        formadd.plan_price.focus();
        return false;
    }
    var plan_period_type = document.forms["formadd"]["plan_period_type"].value;
    if (plan_period_type == "") {
        document.getElementById('plan_period_type_errorbox').style.color='red';
        document.getElementById('plan_period_type_errorbox').innerHTML ="Enter Membership Period";
        formadd.plan_period_type.focus();
        return false;
    }    
    var plan_days = document.forms["formadd"]["plan_days"].value;
    //var regExp    = /^\s*[0-9,\s]+[a-zA-Z,\s]+\s*$/;
    var reg                    = new RegExp('^[0-9]+$');
    if (!plan_days.match(reg)) {
        document.getElementById('plan_days_errorbox').style.color='red';
        document.getElementById('plan_days_errorbox').innerHTML ="Enter Valid Membership Plan Period In Number Format";
        formadd.plan_days.focus();
        return false;
    } 
    if(plan_type != '' && plan_description!='' && plan_price!='' && plan_period_type!= '' && plan_days!='' && plan_productID != '') {
       document.getElementById('plan_type_errorbox').innerHTML ="";
       document.getElementById('plan_productID_errorbox').innerHTML ="";
       document.getElementById('plan_description_errorbox').innerHTML ="";
       document.getElementById('plan_price_errorbox').innerHTML ="";
       document.getElementById('plan_period_type_errorbox').innerHTML ="";
       document.getElementById('plan_days_errorbox').innerHTML ="";
    }           
}  

  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;

    function edit(membership_id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
  
      $.ajax({
        url : "<?php echo base_url()?>getmembershipdata",
        data: {membership_id: membership_id},
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          
             $('[name="membership_id"]').val(data.membership_id);
             $("#membership_type").val(data.membership_type);
             // $('[name="membership_type"]').val(data.membership_type);
             // $('#membership_type option[value="'+data.membership_type+'"]').attr('selected', 'selected');
             $('[name="membership_productID"]').val(data.membership_productID);
             $('[name="membership_description"]').val(data.membership_description);
             $('[name="membership_price"]').val(data.membership_price);
             $('[name="membership_time"]').val(data.membership_time);
             $('[name="membership_period"]').val(data.membership_period);


             $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
             $('.modal-title').text('Edit Membership Data'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



    function save() {

      setTimeout(function(){ $('#membership_type_errorbox').show(); } );
      setTimeout(function(){ $('#membership_productID_errorbox').show(); } );
      setTimeout(function(){ $('#membership_description_errorbox').show(); } );
      setTimeout(function(){ $('#membership_price_errorbox').show(); } );
      setTimeout(function(){ $('#membership_period_errorbox').show(); } );        
      setTimeout(function(){ $('#membership_time_errorbox').show(); } );        

    var membership_type        = $("#membership_type").val(); 
    var membership_productID   = $("#membership_productID").val(); 
    var membership_description = $("#membership_description").val();  
    var membership_price       = $("#membership_price").val();  
    var membership_time        = $("#membership_time").val(); 
    var membership_period      = $("#membership_period").val(); 
    var reg                    = new RegExp('^[0-9]+$');
    // var reg                    = new RegExp(^[0-9]\d{0,9}(\.\d{1,3})?%?$); 
    var regExp                 = /^\s*[0-9,\s]+[a-zA-Z,\s]+\s*$/;
    var regexp = /^[0-9]+([,.][0-9]+)?$/g;


     if(membership_type.length == '') {
        document.getElementById('membership_type_errorbox').style.color='red';
        document.getElementById('membership_type_errorbox').innerHTML ="Select Membership Type";
        form.membership_type.focus();
        return false;
     } 
     if(membership_productID.length == '') {
        document.getElementById('membership_productID_errorbox').style.color='red';
        document.getElementById('membership_productID_errorbox').innerHTML ="Enter Membership Product ID";
        form.membership_productID.focus();
        return false;
     }
     if(membership_description.length == '') {
        document.getElementById('membership_description_errorbox').style.color='red';
        document.getElementById('membership_description_errorbox').innerHTML ="Enter Membership Description";
        form.membership_description.focus();
        return false;
     }
     if(!membership_price.match(regexp)) {
        document.getElementById('membership_price_errorbox').style.color='red';
        document.getElementById('membership_price_errorbox').innerHTML ="Enter Membership Price";
        form.membership_price.focus();
        return false;
     } 
     if(membership_period.length == '') {
        document.getElementById('membership_period_errorbox').style.color='red';
        document.getElementById('membership_period_errorbox').innerHTML ="Select Membership Period";
        form.membership_period.focus();
        return false;
     }           
     if(!membership_time.match(reg)) {
        document.getElementById('membership_time_errorbox').style.color='red';
        document.getElementById('membership_time_errorbox').innerHTML ="Enter Membership Time In Number Format";
        form.membership_time.focus();
        return false;
     }     
     if(membership_type != '' && membership_description != '' && membership_price!= '' && membership_period!= '' && membership_time!= '' && membership_productID != '')  {
        document.getElementById('membership_type_errorbox').innerHTML ="";
        document.getElementById('membership_productID_errorbox').innerHTML ="";
        document.getElementById('membership_description_errorbox').innerHTML ="";
        document.getElementById('membership_price_errorbox').innerHTML ="";
        document.getElementById('membership_period_errorbox').innerHTML ="";
        document.getElementById('membership_time_errorbox').innerHTML ="";
     } 

      document.getElementById('btnSave').disabled = true;
      // ajax adding data to database
          $.ajax({
            url : "<?php echo base_url()?>membership_update",
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
              $('#response').html(data.msg);
              $('#response').show();
              $('#form').hide();
              setTimeout(function(){location.reload();},800);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

   function getRemove() {

      setTimeout(function(){ $('#membership_type_errorbox').hide(); } );
      setTimeout(function(){ $('#membership_productID_errorbox').hide(); } );
      setTimeout(function(){ $('#membership_description_errorbox').hide(); } );
      setTimeout(function(){ $('#membership_price_errorbox').hide(); } );
      setTimeout(function(){ $('#membership_period_errorbox').hide(); } );        
      setTimeout(function(){ $('#membership_time_errorbox').hide(); } );        
  }
  </script>

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" onclick="getRemove();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Membership</h3>
         <h4 id="response" style="display:none; margin: 0 0 5px 0; padding: 8px; background-color: #00a65a; color:white;">
         </h4>
      </div>
      <div class="modal-body form membership-modal">
        <form action="#" id="form" class="form-horizontal" name="form">
          <input type="hidden" value="" name="membership_id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label">Membership Type</label>
              <div class="col-md-12">
                <select id="membership_type" required="required" class="form-control col-md-7 col-xs-12" name="membership_type">
                             <option value="Monthly">Monthly</option>
                             <option value="Token">Token</option>

               </select>
              </div>
              <div class="col-md-12"></div>
              <div id="membership_type_errorbox" class="col-md-12"></div>
            </div>

            <div class="form-group">
              <label class="control-label">Membership Product ID</label>
              <div class="col-md-12">
              <input name="membership_productID" id="membership_productID" placeholder="Membership Product ID" class="form-control" type="text" disabled>
              </div>
              <div class="col-md-12"></div>
              <div id="membership_productID_errorbox" class="col-md-12"></div>
            </div>

            <div class="form-group">
              <label class="control-label">Membership Description</label>
              <div class="col-md-12">
              <input name="membership_description" id="membership_description" placeholder="Membership Description" class="form-control" type="text">
              </div>
              <div class="col-md-12"></div>
              <div id="membership_description_errorbox" class="col-md-12"></div>
            </div>

            <div class="form-group">
              <label class="control-label">Membership Price</label>
              <div class="col-md-12">
                <input name="membership_price" id="membership_price" placeholder="Membership Price" class="form-control" type="text">
              </div>
              <div class="col-md-12"></div>
              <div id="membership_price_errorbox" class="col-md-12"></div>
            </div>

            <div class="form-group">
              <label class="control-label">Membership Period </label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <select name="membership_period" id="membership_period" class="form-control col-md-7 col-xs-12">
                        <option value="">Select Membership Period</option>
                        <option value="min">Minutes</option>
                        <option value="hr">Hours</option>
                        <option value="day">Days</option>
                        <option value="month">Months</option>
                    </select>
                </div>
                <div class="col-md-12"></div>
                <div id="membership_period_errorbox" class="col-md-12"></div>
            </div>              

            <div class="form-group">
              <label class="control-label">Membership Time</label>
              <div class="col-md-12">
                <input name="membership_time" id="membership_time" placeholder="Membership Days" class="form-control" type="text">
              </div>
              <div class="col-md-12"></div>
              <div id="membership_time_errorbox" class="col-md-12"></div>
            </div>

          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-info">Save</button>
            <button type="button" onclick="getRemove();" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->



     <!-- jQuery -->
    <script src="<?php echo base_url();?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>

  </body>
</html>
