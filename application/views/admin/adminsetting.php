        <!-- page content -->
     <div class="right_col" role="main">
          <!--<div class="page-title">
              <div class="title_left">
                <h3>Admin Setting</h3>
              </div>
            </div>
            <div class="clearfix"></div>-->
                
           <div class="row adminsetting">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
				   <h2>Admin Setting</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>-->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                   
                  
                  <div class="x_content">
                  <?php if($this->session->flashdata('success')){  ?>
                      <b style="color:green; padding:10px; font-size:15px;">
                       <?php echo $this->session->flashdata('success'); } ?>
                      </b>
                  <?php if($this->session->flashdata('error')){  ?>
                      <b style="color:red; padding:10px; font-size:15px;">
                       <?php echo $this->session->flashdata('error'); } ?>
                      </b>    
                  
                   <?php //print_r($set); ?>
                    <form name="form" id="form" method="post" action="<?php echo base_url() ?>adminsettingupdate"  data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label">Email
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12" value="<?php echo $set[0]['admin_set_email']; ?>" disabled>
                        </div>
                        <div class="col-md-12"></div>
                        <div id="email_errorbox"  class="col-md-12"></div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Contact
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="contact" name="contact" class="form-control col-md-7 col-xs-12" value="<?php echo $set[0]['admin_set_number']; ?>" disabled maxlength="10">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="contact_errorbox"  class="col-md-12"></div>
                      </div>
 
                      <div class="form-group">
                        <label class="control-label">Address
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea id="address" rows="2" name="address" class="form-control col-md-7 col-xs-12" disabled><?php echo $set[0]['admin_set_address']; ?></textarea> 
                        </div>
                        <div class="col-md-12"></div>
                        <div id="address_errorbox"  class="col-md-12"></div>
                      </div>

                     <div class="form-group">
                        <label class="control-label">
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="button" id="button1" value="Edit" class="btn btn-warning" onclick="enableButton2()" />
                        <input type="submit" class="btn btn-success" id="button2" onclick="return vali();" value="Update" disabled>
                        </div>
                      </div>

                    </form>
                                    
                  </div>
                </div>
              </div>
            </div>
     </div>
        <!-- </div> -->
        <!-- /page content -->


    <script type="text/javascript">
    
    function enableButton2() {
            document.getElementById("button2").disabled = false;
            document.getElementById("contact").disabled = false;
            document.getElementById("email").disabled = false;
            document.getElementById("address").disabled = false;
            document.getElementById("button1").disabled = true;
        }

   function vali() {

    var email = form.email.value;
    var contact = form.contact.value;
    var address = form.address.value;
    var mailformat   = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var numbers = /^[0-9]+$/;


      if(!email.match(mailformat)) {
        document.getElementById('email_errorbox').style.color='red';
        document.getElementById('email_errorbox').innerHTML ="Enter valid mail id";
        form.email.focus();
        return false;
      }       
      if(!contact.match(numbers)) {
        document.getElementById('contact_errorbox').style.color='red';
        document.getElementById('contact_errorbox').innerHTML ="Enter valid contact number";
        form.contact.focus();
        return false;
      } 
      if(address.length == '') {
        document.getElementById('address_errorbox').style.color='red';
        document.getElementById('address_errorbox').innerHTML ="Enter Address";
        form.address.focus();
        return false;
      }      
      if(email != '' && contact !='' && address!= '') {
        document.getElementById('email_errorbox').innerHTML ="";
        document.getElementById('contact_errorbox').innerHTML ="";
        document.getElementById('address_errorbox').innerHTML ="";
      }                        
   }         
  </script>


     <!-- jQuery -->
    <script src="<?php echo base_url();?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>

  </body>
</html>
