        <!-- page content -->
     <div class="right_col" role="main">
          
            <div class="clearfix"></div>
                   <?php if($this->session->flashdata('success')){  ?>
                      <b style="color:green; padding:10px; font-size:15px;">
                       <?php echo $this->session->flashdata('success'); } ?>
                      </b>
                   <?php if($this->session->flashdata('error')){  ?>
                      <b style="color:red; padding:10px; font-size:15px;">
                       <?php echo $this->session->flashdata('error'); } ?>
                      </b>  

            <div class="row profilepage">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Profile</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>-->
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                     
                  
                  
                    <form method="post" action="<?php echo base_url() ?>profileupdate"  data-parsley-validate class="form-horizontal form-label-left" name="formadd">

                    <input type="hidden" name="id" name="id" value="<?php echo $admin[0]['userId']; ?>">

                      <div class="form-group">
                        <label class="control-label">Name </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="name" name="name" value="<?php echo $admin[0]['name']; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="name_errorbox"  class="col-md-12"></div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Email </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="email" name="email" value="<?php echo $admin[0]['email']; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="email_errorbox"  class="col-md-12"></div>
                      </div>
 
                      <div class="form-group">
                        <label class="control-label">Mobile</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" id="mobile" name="mobile" value="<?php echo $admin[0]['mobile']; ?>" class="form-control col-md-7 col-xs-12" maxlength="10">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="mobile_errorbox"  class="col-md-12"></div>
                      </div>

                      <div class="form-group">
                        <label class="control-label"> </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="submit" class="btn btn-success" value="Submit" onclick="return vali();">
                        </div>
                      </div>

                    </form>
                   
                  </div>
                </div>
              </div>
			  
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Change Password</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>-->
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                      <?php if($this->session->flashdata('successpass')){  ?>
                      <b style="color:green; padding:10px; font-size:15px;">
                       <?php echo $this->session->flashdata('successpass'); } ?>
                      </b>
                   <?php if($this->session->flashdata('errorpass')){  ?>
                      <b style="color:red; padding:10px; font-size:15px;">
                       <?php echo $this->session->flashdata('errorpass'); } ?>
                      </b> 
                 
                   
                    <form method="post" action="<?php echo base_url() ?>changepassword"  data-parsley-validate class="form-horizontal form-label-left" name="form">

                        <input type="hidden" name="id" name="id" value="<?php echo $admin[0]['userId']; ?>">

                      <div class="form-group">
                        <label class="control-label">Old Password</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="password" id="old_pass" name="old_pass" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="old_pass_errorbox"  class="col-md-12"></div>
                      </div>

                      <div class="form-group">
                        <label class="control-label">New Password </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="password" id="new_pass" name="new_pass" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="new_pass_errorbox"  class="col-md-12"></div>
                      </div>
 
                      <div class="form-group">
                        <label class="control-label">Confirm Password</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                         <input type="password" id="confirm_pass" name="confirm_pass" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-12"></div>
                        <div id="confirm_pass_errorbox"  class="col-md-12"></div>
                      </div>
 
                     <div class="form-group">
                        <label class="control-label">
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="submit" class="btn btn-success" value="Submit" onclick="return validation();">
                        </div>
                      </div>

                    </form>
                   
                  </div>
                </div>
              </div>
			  
            </div>

          
     </div>

<script type="text/javascript">
  
   function validation() {

    var  old_pass = form.old_pass.value;
    var  new_pass = form.new_pass.value;
    var  confirm_pass = form.confirm_pass.value;

      if(old_pass.length == '') {
        document.getElementById('old_pass_errorbox').style.color='red';
        document.getElementById('old_pass_errorbox').innerHTML ="Enter Old Password";
        form.old_pass.focus();
        return false;
      }
      if(new_pass.length < 6) {
        document.getElementById('new_pass_errorbox').style.color='red';
        document.getElementById('new_pass_errorbox').innerHTML ="Enter New Password With Minimum 6 Characters";
        form.new_pass.focus();
        return false;
      }
      if(confirm_pass.length < 6) {
        document.getElementById('confirm_pass_errorbox').style.color='red';
        document.getElementById('confirm_pass_errorbox').innerHTML ="Enter Confirm Password With Minimum 6 Characters";
        form.confirm_pass.focus();
        return false;
      }
      if(new_pass != confirm_pass) {
        document.getElementById('confirm_pass_errorbox').style.color='red';
        document.getElementById('confirm_pass_errorbox').innerHTML ="New Password And Confirm Password Is Mismatched";
        form.confirm_pass.focus();
        return false;
      } 
      if(old_pass != '' && new_pass !='' && confirm_pass!= '') {
        document.getElementById('old_pass_errorbox').innerHTML ="";
        document.getElementById('new_pass_errorbox').innerHTML ="";
        document.getElementById('confirm_pass_errorbox').innerHTML ="";
      }                        


   }


   function vali() {

    var name = formadd.name.value;
    var email = formadd.email.value;
    var mobile = formadd.mobile.value;
    var mailformat   = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var numbers = /^[0-9]+$/;

      if(name.length == '') {
        document.getElementById('name_errorbox').style.color='red';
        document.getElementById('name_errorbox').innerHTML ="Enter Name";
        formadd.name.focus();
        return false;
      }
      if(!email.match(mailformat)) {
        document.getElementById('email_errorbox').style.color='red';
        document.getElementById('email_errorbox').innerHTML ="Enter valid mail id";
        formadd.email.focus();
        return false;
      }       
      if(!mobile.match(numbers)) {
        document.getElementById('mobile_errorbox').style.color='red';
        document.getElementById('mobile_errorbox').innerHTML ="Enter valid contact number";
        formadd.mobile.focus();
        return false;
      } 
      if(name != '' && email !='' && mobile!= '') {
        document.getElementById('name_errorbox').innerHTML ="";
        document.getElementById('email_errorbox').innerHTML ="";
        document.getElementById('mobile_errorbox').innerHTML ="";
      }                        
   }   

</script>

     <!-- jQuery -->
    <script src="<?php echo base_url();?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>

  </body>
</html>
