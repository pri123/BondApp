
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User Management <!-- <small>Users</small> --></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <?php if($this->session->flashdata('success')){  ?>
                  <h4 class="hide-it col-md-6" style="margin: 0 0 5px 0; padding: 8px; background-color: #00a65a; color:white;">
                          <?php echo $this->session->flashdata('success'); } ?>
                  </h4>
                  </div>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Sr No.</th>
                          <th>Name</th>
                          <th>Mobile number</th>
                          <th>Subscription</th>
                          <th>Account Status</th>
                          <th>Action</th>
                          <th>Country</th>
                          <th>Created</th>
                        </tr>
                      </thead>


                      <tbody>
                     
                     <?php $i=1; foreach ($AllUsersdata as $key) {  

                      
                      ?>
                      
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $key['user_firstname']." ".substr($key['user_lastname'],0,1); ?></td>
                          <td><?php echo $key['user_mobileno']; ?></td>
                          <td><?php if($key['user_subscription'] == 0) { echo "Free"; } else { echo "Paid"; } ?></td>
                          <td><?php if($key['user_status'] == 0) { echo "Active"; } else { echo "Deactive"; } ?></td>
                          <td>
                            <a href="<?php echo base_url(); ?>user/deleteUserManagement/<?php echo $key['user_id'] ?>" class="btn btn-warning"><i class="fa fa-trash-o " aria-hidden="true" title="Delete"> Delete</i></a>
                            <?php if($key['user_status'] == 0) { ?>

                            <a  href="<?php echo base_url(); ?>user/deactivateUserManagement/<?php echo $key['user_id']; ?>" class="btn btn-danger"><i class="fa fa-ban" aria-hidden="true" title="Deactivate"> Deactivate</i></a>

                            <?php } else { ?>

                            <a  href="<?php echo base_url(); ?>user/activateUserManagement/<?php echo $key['user_id']; ?>" class="btn btn-success"><i class="fa fa-check" aria-hidden="true" title="Activate"> Activate</i></a>

                            <?php } ?>
                          </td>
                          
                          <td><?php echo countryCode($key['user_country_code']); ?> </td>
                          <td><?php echo $key['user_created_at']; ?> </td>
                        </tr>
                     <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- </div> -->
        <!-- /page content -->

      
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>

  </body>
</html>
