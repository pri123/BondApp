<script>
 function change_tab(id)
 {
   document.getElementById("page_content").innerHTML=document.getElementById(id+"_desc").innerHTML;
   document.getElementById("page1").className="notselected";
   document.getElementById("page2").className="notselected";
   document.getElementById(id).className="selected";
 }
</script>
<script src="<?php echo base_url(); ?>assets/js/bar.js"></script>

<style>
#main_content
{
 margin-top:50px;
 
}
#main_content li
{
 display:inline;
 list-style-type:none;
 padding:14px;
 border-radius:5px 5px 0px 0px;
 color:#292A0A;
 font-weight:bold;
 cursor:pointer;
}
#main_content li.notselected
{
 background-color:#BFC9CA;
 color:#292A0A; 
}
#main_content li.selected
{
 background-color:#797D7F;
 color:#292A0A; 
}
#main_content .hidden_desc
{
 display:none;
 visibility:hidden;
}
#main_content #page_content
{
  
 padding:10px;
 margin-top:9px;
 border-radius:0px 5px 5px 5px;
 color:#2E2E2E;
 line-height: 1.6em;
 word-spacing:4px;
}
</style>

        <!-- page content -->
        <div class="right_col statistics" role="main">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Statistics<!-- <small>Users</small> --></h2>                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">  
                  <link rel="stylesheet" type="text/css" href="tabs_style.css">

<div id="main_content">

 <li class="selected" id="page1" onclick="change_tab(this.id);">Token</li>
 <li class="notselected" id="page2" onclick="change_tab(this.id);">Monthly</li>
 <li id="page3" onclick="change_tab(this.id);"></li>
 <li id="page4" onclick="change_tab(this.id);"></li>
 
 
 <div class='hidden_desc' id="page1_desc">
    <div class="col-md-5 col-sm-5 col-xs-12">
         <h3>Top Regions</h3>
                   <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Country</th>
                          <th>View Status</th>
                     </thead>
                     <tbody>
                          <?php foreach ($Token as $key) { ?>
                          <tr>
                              <td><?php echo countryCode($key['user_country_code']); ?></td>
                              <td><?php echo round($key['count'] / $purchaseCount *100,2)." %" ; ?></td>
                          </tr>
                          <?php } ?>
                     </tbody>
                  </table>      
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12" >
    <h3>Gender</h3>
    <div id="containerHidden" style=" width:250px; height: 300px; margin-top:40px;"></div>   
      <!--  <h3>By Gender</h3>
                  <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Gender</th>
                          <th>Count</th>
                      </thead>

                      <tbody>
                       <?php  foreach ($genderCountToken as $key) { ?>
                        <tr>
                          <td><?php echo $key->user_gender; ?></td>
                          <td><?php echo $key->count; ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                  </table>      -->

    </div> 

    <div class="col-md-2 col-sm-2 col-xs-12">
      <a class="btn btn-info" href="<?php echo base_url(); ?>user/ExportCSVToken">Download (Token Users)</a>
    </div>        
 </div>

 <div class='hidden_desc' id="page2_desc">
  
    <div class="col-md-5 col-sm-5 col-xs-12">

       <h3>Top Regions</h3>
                  <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Country</th>
                          <th>View Status</th>
                      </thead>
                      <tbody>
                          <?php foreach ($Monthly as $key) { ?>
                          <tr>
                              <td><?php echo countryCode($key['user_country_code']); ?></td>
                              <td><?php echo round($key['count'] / $purchaseCount *100 ,2)." %" ; ?></td>
                          </tr>
                          <?php } ?>
                      </tbody>
                  </table>      
    </div>

     <div class="col-md-4 col-sm-4 col-xs-12" >
        <h3>Gender</h3>
        <div id="container1" style=" width:250px; height: 300px; margin-top:40px;"></div>   

    </div> 

    <div class="col-md-2 col-sm-2 col-xs-12">
      <a class="btn btn-info" href="<?php echo base_url(); ?>user/ExportCSVMonthly">Download (Monthly Users)</a>
    </div> 
 </div>
 
 <div id="page_content" class="col-md-12 col-sm-12 col-xs-12">

     <div class="col-md-5 col-sm-5 col-xs-12">
         <h3>Top Regions</h3>
                  <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Country</th>
                          <th>View Status</th>
                      </thead>
                      <tbody>
                        <?php foreach ($Token as $key) { ?>
                        <tr>
                            <td><?php echo countryCode($key['user_country_code']); ?></td>
                            <td><?php echo round($key['count'] / $purchaseCount *100,2)." %" ; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                  </table>      
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12" >
    <h3>Gender</h3>
    <div id="container" style=" width:250px; height: 300px; margin-top:40px;"></div>   
    </div> 

    <div class="col-md-2 col-sm-2 col-xs-12">
      <a class="btn btn-info" href="<?php echo base_url(); ?>user/ExportCSVToken">Download (Token Users)</a>
    </div>  
     </div>
    </div>                  
   </div>
  </div>
 </div>
</div>
</div>
<!-- </div> -->
<!-- /page content -->


      </div>
    </div>

     <!-- jQuery -->
    <script src="<?php echo base_url();?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script>
// Token

 Highcharts.chart('container', {
  chart: { type: 'column' },
    xAxis: { type: 'category' },
    yAxis: { title: { text: 'Total percent' } },
    legend: { enabled: false },
    plotOptions: { series: { borderWidth: 0, dataLabels: { enabled: true, format: '{point.y:1f}%' } } },

    series: [{
        name: 'Gender', colorByPoint: true,
        data: [{ name: 'Male', y: <?php echo $genderCountToken[1]->count; ?>, drilldown: 'Male' }, { name: 'Female', y: <?php echo $genderCountToken[0]->count; ?>, drilldown: 'Female' }]
    }]
});

</script>

<script>
// Token
 
 Highcharts.chart('containerHidden', {
    chart: { type: 'column' },
    xAxis: { type: 'category' },
    yAxis: { title: { text: 'Total percent' } },
    legend: { enabled: false },
    plotOptions: { series: { borderWidth: 0, dataLabels: { enabled: true, format: '{point.y:1f}%' } } },

    series: [{
        name: 'Gender', colorByPoint: true,
        data: [{ name: 'Male', y: <?php echo $genderCountToken[1]->count; ?>, drilldown: 'Male' }, { name: 'Female', y: <?php echo $genderCountToken[0]->count; ?>, drilldown: 'Female' }]
    }]
});

</script>

<script>
// Monthly
 
 Highcharts.chart('containerMonthly', {
    chart: { type: 'column' },
    xAxis: { type: 'category' },
    yAxis: { title: { text: 'Total percent' } },
    legend: { enabled: false },
    plotOptions: { series: { borderWidth: 0, dataLabels: { enabled: true, format: '{point.y:1f}%' } } },

    series: [{
        name: 'Gender', colorByPoint: true,
        data: [{ name: 'Male', y: <?php echo $genderCountMonthly[1]->count; ?>, drilldown: 'Male' }, { name: 'Female', y: <?php echo $genderCountMonthly[0]->count; ?>, drilldown: 'Female' }]
    }]
});

</script>

  </body>
</html>
